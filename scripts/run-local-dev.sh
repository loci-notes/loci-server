#! /usr/bin/env bash

# Exit in case of error
set -e

# If .env file does exist, create it
if [ ! -f .env ]; then
    cp .env_EXAMPLE_DEV .env
fi

docker-compose -f docker-compose.DEV.yml down -v --remove-orphans # Remove possibly previous broken stacks left hanging after an error

docker-compose -f docker-compose.DEV.yml build
docker-compose -f docker-compose.DEV.yml up -d

