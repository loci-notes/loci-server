# Loci Notes Server
The core server of the [Loci Notes system](https://loci-notes.gitlab.io/).

Generated from [the FastAPI Boilerplate project](https://github.com/tiangolo/full-stack-fastapi-template/tree/b6f37e4fb763b1c507dbd214e40fcb1f70e0398f). 

:warning: WARNING: THE LOCI NOTES SERVER IS CURRENTLY UNDERGOING SIGNIFICANT DEVELOPMENT AND CHANGES! UNLESS YOU ARE VERY SURE YOU KNOW HOW TO HANDLE THIS, YOU SHOULD USE [LOCI-SERVER VERSION 0.14.0](https://gitlab.com/loci-notes/loci-server/-/tree/v0.14.0?ref_type=tags) UNTIL THIS WARNING IS REMOVED FROM THIS PAGE! :warning:

## Technology Stack and Features

- ⚡ [**FastAPI**](https://fastapi.tiangolo.com) for the Python backend API.
    - 🧰 [SQLModel](https://sqlmodel.tiangolo.com) for the Python SQL database interactions (ORM).
    - 🔍 [Pydantic](https://docs.pydantic.dev), used by FastAPI, for the data validation and settings management.
    - 💾 [PostgreSQL](https://www.postgresql.org) as the SQL database.
- 🚀 [React](https://react.dev) for the frontend.
    - 💃 Using TypeScript, hooks, Vite, and other parts of a modern frontend stack.
    - 🎨 [Chakra UI](https://chakra-ui.com) for the frontend components.
    - 🤖 An automatically generated frontend client.
    - 🦇 Dark mode support.
- 🐋 [Docker Compose](https://www.docker.com) for development and production.
- 🔒 Secure password hashing by default.
- 🔑 JWT token authentication.
- 📫 Email based password recovery.
- ✅ Tests with [Pytest](https://pytest.org).
- 📞 [Traefik](https://traefik.io) as a reverse proxy / load balancer.
- 🚢 Deployment instructions using Docker Compose, including how to set up a frontend Traefik proxy to handle automatic HTTPS certificates.
- 🏭 CI (continuous integration) and CD (continuous deployment) based on GitHub Actions.

### Configure

You can then update configs in the `.env` files to customize your configurations.

Before deploying it, make sure you change at least the values for:

- `SECRET_KEY`
- `FIRST_SUPERUSER_PASSWORD`
- `POSTGRES_PASSWORD`

### Generate Secret Keys

Some environment variables in the `.env` file have a default value of `changethis`.

You have to change them with a secret key, to generate secret keys you can run the following command:

```bash
python -c "import secrets; print(secrets.token_urlsafe(32))"
```

Copy the content and use that as password / secret key. And run that again to generate another secure key.

## Installation
The officially supported deployment method is [Docker Compose](https://docs.docker.com/compose/). Other methods like Kubernetes, Docker without Compose, or manual deployment are possible, but harder.

If you want to deploy Loci Notes to a remote Docker/Docker compose host rather than your current host (without copying over the codebase), use [docker contexts](https://www.docker.com/blog/how-to-deploy-on-remote-docker-hosts-with-docker-compose/).

### Docker Compose (Easy)
1. Login to your Docker Compose host.
2. Copy the `.env_EXAMPLE` and `docker-compose.yml` file to the host. You do not need the rest of the source code.
3. Rename `.env_EXAMPLE` to `.env`. Replace the all instances of `changethis` with something new and secure. Usually, you can deploy Loci Notes in this way with no other changes to the `.env` file.
4. Run `docker compose up -d`.


## Backend Development

Backend docs: [backend/README.md](./backend/README.md).

## Frontend Development

Frontend docs: [frontend/README.md](./frontend/README.md).

## Deployment

Deployment docs: [deployment.md](./deployment.md).

## Development

General development docs: [development.md](./development.md).

This includes using Docker Compose, custom local domains, `.env` configurations, etc.

## Release Notes

Check the file [release-notes.md](./release-notes.md).

## License

The Full Stack FastAPI Template is licensed under the terms of the MIT license.
