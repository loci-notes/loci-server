#!/usr/bin/env bash

set -e
set -x

mypy app

# These two lines will autofix anything the below two find. You should not run this is CICD.
# ruff app --fix
# ruff format app

# The following two lines will just CHECK to make sure everything is linting correctly.
ruff app
ruff format app --check