from datetime import datetime
from enum import Enum

from sqlmodel import Field, Relationship, SQLModel


class OrderByEnum(str, Enum):
    asc = "asc"
    desc = "desc"


# This is a link table between users and projects, as detailed in
# https://sqlmodel.tiangolo.com/tutorial/many-to-many/create-models-with-link/
class ProjectUserAccess(SQLModel, table=True):
    project_id: int | None = Field(
        default=None, foreign_key="project.id", primary_key=True
    )
    user_id: int | None = Field(default=None, foreign_key="user.id", primary_key=True)


class ProjectManagerAccess(SQLModel, table=True):
    project_id: int | None = Field(
        default=None, foreign_key="project.id", primary_key=True
    )
    user_id: int | None = Field(default=None, foreign_key="user.id", primary_key=True)


# Shared properties
# TODO replace email str with EmailStr when sqlmodel supports it
class UserBase(SQLModel):
    email: str = Field(unique=True, index=True)
    is_active: bool = True
    is_superuser: bool = False
    full_name: str | None = None


# Properties to receive via API on creation
class UserCreate(UserBase):
    password: str


# TODO replace email str with EmailStr when sqlmodel supports it
class UserCreateOpen(SQLModel):
    email: str
    password: str
    full_name: str | None = None


# Properties to receive via API on update, all are optional
# TODO replace email str with EmailStr when sqlmodel supports it
class UserUpdate(UserBase):
    email: str | None = None  # type: ignore
    password: str | None = None


# TODO replace email str with EmailStr when sqlmodel supports it
class UserUpdateMe(SQLModel):
    full_name: str | None = None
    email: str | None = None


class UpdatePassword(SQLModel):
    current_password: str
    new_password: str


# Database model, database table inferred from class name
class User(UserBase, table=True):
    id: int | None = Field(default=None, primary_key=True)
    hashed_password: str
    api_keys: list["ApiKey"] = Relationship(back_populates="owner")
    projects: list["Project"] = Relationship(
        back_populates="users", link_model=ProjectUserAccess
    )
    managed_projects: list["Project"] = Relationship(
        back_populates="managers", link_model=ProjectManagerAccess
    )
    notes: list["ArtifactNote"] = Relationship(back_populates="author")


# Properties to return via API, id is always required
class UserOut(UserBase):
    id: int


class UserOutWithProjects(UserOut):
    id: int
    projects: list["ProjectOut"]
    managed_projects: list["ProjectOut"]


class UserFullOut(UserOut):
    is_active: bool
    is_superuser: bool
    api_keys: list["ApiKeyOut"]
    projects: list["ProjectOut"]
    managed_projects: list["ProjectOut"]


class UsersOut(SQLModel):
    data: list[UserOut]
    count: int


# Generic message
class Message(SQLModel):
    message: str


# JSON payload containing access token
class Token(SQLModel):
    access_token: str
    token_type: str = "bearer"


# Contents of JWT token
class TokenPayload(SQLModel):
    sub: int | None = None


class NewPassword(SQLModel):
    token: str
    new_password: str


class StatusOut(SQLModel):
    version: str
    env: str
    now: datetime
    open_reg: bool


class ApiKeySortEnum(str, Enum):
    created_at = "created_at"
    client_id = "client_id"


class ApiKeyBase(SQLModel):
    client_id: str
    # This is just a user-readable description of the API key. It is not important what
    # is in it.
    description: str | None = None


class ApiKeyIn(SQLModel):
    description: str | None = None


class ApiKey(ApiKeyBase, table=True):
    id: int | None = Field(default=None, primary_key=True)
    # This secret_key_hash is a hash of the secret_key, not the secret key itself.
    secret_key_hash: str
    owner_id: int | None = Field(default=None, foreign_key="user.id", nullable=False)
    owner: User | None = Relationship(back_populates="api_keys")
    created_at: datetime = Field(default_factory=datetime.utcnow)


# Properties to return via API
class ApiKeyOut(ApiKeyBase):
    id: int
    owner_id: int
    created_at: datetime
    # Properties to return via API, this one with the secret key in it (which you'll
    # only ever get at creation time)
    secret_key: str | None = None


class ApiKeysOut(SQLModel):
    data: list[ApiKeyOut]
    count: int


class ProjectSortEnum(str, Enum):
    created_at = "created_at"
    name = "name"


class ProjectIn(SQLModel):
    name: str


class Project(ProjectIn, table=True):
    id: int | None = Field(default=None, primary_key=True)
    created_at: datetime = Field(default_factory=datetime.utcnow)
    users: list["User"] = Relationship(
        back_populates="projects", link_model=ProjectUserAccess
    )
    managers: list["User"] = Relationship(
        back_populates="managed_projects", link_model=ProjectManagerAccess
    )
    artifacts: list["Artifact"] = Relationship(back_populates="project")


class ProjectOut(ProjectIn):
    id: int
    created_at: datetime
    users: list[UserOut]
    managers: list[UserOut]


class ProjectsOut(SQLModel):
    data: list[ProjectOut]
    count: int


class ProjectAccessUser(SQLModel):
    id: int
    is_manager: bool | None = False


class ArtifactPriorityEnum(str, Enum):
    HIGH = "HIGH"
    MEDIUM = "MEDIUM"
    LOW = "LOW"
    NONE = "NONE"


class ArtifactTypeEnum(str, Enum):
    # This is simply an artifact that holds a file location, but not it's contents.
    # The contents are stored in a seprate object storage server, usually Minio or S3.
    # Examples:
    # - repo/file.txt
    # - repo-master/path/file.txt
    # - file.txt
    SOURCE_CODE_FILE = "SOURCE_CODE_FILE"

    # This describes a location in the source code. This is usuaully a source code
    # file artifact (from above, plus one or more line numbers).
    # Examples:
    # - repo/file.txt:1
    # - repo-master/path/file.txt:1-10
    # - file.txt:1-10
    SOURCE_CODE_LOCATION = "SOURCE_CODE_LOCATION"

    # This is a general purpose artifact that doesn't fit into any other category.
    OTHER = "OTHER"


class ArtifactSortEnum(str, Enum):
    created_at = "created_at"
    descriptor = "descriptor"


class ArtifactBase(SQLModel):
    descriptor: str
    priority: ArtifactPriorityEnum = ArtifactPriorityEnum.MEDIUM
    type: ArtifactTypeEnum = ArtifactTypeEnum.OTHER


class Artifact(ArtifactBase, table=True):
    id: int | None = Field(default=None, primary_key=True)
    descriptor: str = Field(index=True)

    project_id: int | None = Field(
        default=None, foreign_key="project.id", nullable=False
    )
    project: Project = Relationship(back_populates="artifacts")

    created_at: datetime = Field(default_factory=datetime.utcnow)
    updated_at: datetime = Field(default_factory=datetime.utcnow)
    notes: list["ArtifactNote"] = Relationship(back_populates="artifact")


class ArtifactIn(SQLModel):
    priority: ArtifactPriorityEnum | None = None
    type: ArtifactTypeEnum | None = None


class ArtifactOut(ArtifactBase):
    id: int
    project_id: int
    descriptor: str
    priority: ArtifactPriorityEnum
    type: ArtifactTypeEnum
    created_at: datetime
    updated_at: datetime
    note_ids: list[int]


class ArtifactsOut(SQLModel):
    data: list[ArtifactOut]
    count: int


class ArtifactNoteTypeEnum(str, Enum):
    # A comment is something that an analyst would type in directly pertaining to the
    # artifact.
    COMMENT = "COMMENT"
    # A snapshot is anything that describes the artifact in more detail. This may not
    # always make sense, but could be used to hold snippets of code, for example.
    SNAPSHOT_TXT = "SNAPSHOT_TXT"
    SNAPSHOT_PNG = "SNAPSHOT_PNG"

    # This is a metadata key value pair used to describe the artifact. This is of the
    # form key:value. The key is a string and the value is a string. The
    # key is not unique.
    METADATA_KV = "METADATA_KV"


class ArtifactNoteSortEnum(str, Enum):
    created_at = "created_at"


class ArtifactNoteBase(SQLModel):
    type: ArtifactNoteTypeEnum
    submission_tool: str | None = None
    contents: str


class ArtifactNote(ArtifactNoteBase, table=True):
    id: int | None = Field(default=None, primary_key=True)

    artifact_id: int | None = Field(
        default=None, foreign_key="artifact.id", nullable=False
    )
    artifact: Artifact | None = Relationship(back_populates="notes")

    author_id: int | None = Field(default=None, foreign_key="user.id", nullable=False)
    author: User = Relationship(back_populates="notes")

    created_at: datetime = Field(default_factory=datetime.utcnow)
    updated_at: datetime = Field(default_factory=datetime.utcnow)


class ArtifactNoteIn(SQLModel):
    # Either the artifact_id or the descriptor is required, but not both.
    artifact_id: int | None = None
    artifact_descriptor: str | None = None

    # Notes can set the parent artifact's priority and/or type.
    artifact_type: ArtifactTypeEnum | None = None
    artifact_priority: ArtifactPriorityEnum | None = None

    submission_tool: str | None = None
    type: ArtifactNoteTypeEnum
    contents: str


class ArtifactNoteUpdateIn(SQLModel):
    contents: str | None = None
    type: ArtifactNoteTypeEnum | None = None
    submission_tool: str | None = None


class ArtifactNoteShortOut(SQLModel):
    id: int


class ArtifactNoteExport(ArtifactNoteBase):
    type: ArtifactNoteTypeEnum
    author_id: int
    submission_tool: str | None = None
    contents: str
    created_at: datetime
    updated_at: datetime


class ArtifactExport(SQLModel):
    descriptor: str
    priority: ArtifactPriorityEnum
    type: ArtifactTypeEnum
    created_at: datetime
    notes: list[ArtifactNoteExport]


class ProjectExport(SQLModel):
    export_version: str = "0.1"
    export_time: datetime
    project: ProjectIn
    users: list[UserOut]
    artifacts: list[ArtifactExport]


class ArtifactFileLocationOut(SQLModel):
    filename: str
    link: str
