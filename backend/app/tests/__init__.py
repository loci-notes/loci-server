import json

from app.main import app

# This will write the OpenAPI schema to a file anytime tests are run.
with open("openapi.json", "w") as fd:
    fd.write(json.dumps(app.openapi()))
