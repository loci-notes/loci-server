import base64
import re
from time import sleep

from fastapi.testclient import TestClient
from sqlmodel import Session

from app.core.config import settings

# repo-1
# ├── folder1
# │   └── test2.txt
# └── test.txt
TEST_REPO_1_ZIP = "UEsDBBQAAAAAAMOrNVkAAAAAAAAAAAAAAAAHACAAcmVwby0xL1VUDQAHPo/vZj6P72Y+j+9mdXgLAAEE9QEAAAQUAAAAUEsDBBQACAAIAMerNVkAAAAAAAAAAAQYAAAQACAAcmVwby0xLy5EU19TdG9yZVVUDQAHRo/vZiGP72ZGj+9mdXgLAAEE9QEAAAQUAAAA7ZhLSsNAGID/SaOdUIQsFLoccFuwEfoAN7HWtWBEF63UpoltMM2EprWLUqhbvYNXcOsJPIIX8CImzV+IpgUFwaLzwfAlmX9eCcwjAEBqI0sDUAGAQmxJgaVQTCkkdDZMZF6HDQPQzHHgm+7yugRrRnaeroGDC1by+3EzzHk2fdcJhsXiK5Ey8sZmlmZpjl4aPT42hu3hKKi1B43o7oxz11xct81zxx631O0j7g3bjmcP5gUcyw5DmheOZ/FxjY88K2gkMqhClZaan0zKWrXAqpVpgU20/VKxwMqlynSq0J3dvYPT1o3b9/h93HlCcBRbn0b1kB6VFXRcM+wlSWe5Xc3o8L6PhZdE9Hm9jq+E3j0+Nd9OXg6XRVlfiPJ7ydZUSEfcGgPP5V4X+yIQCAQ/Dk4uNPe73RAIBGtIND8wtI6exSaYL6HlRBkVzdA6ehabYJyEltEUraIZWkfPYuOkRfDwQbBlgicUoqIZWv/WkAWCf0Mmlhqt/8erz/8CgeAPQ+S6Ua/B6r8N0VrLwnS1KAAfNwL4TMbYaCnOJ54ztI6exRYbAYHgt3gHUEsHCHzgFYGqAQAABBgAAFBLAwQUAAgACADHqzVZAAAAAAAAAAB4AAAAGwAgAF9fTUFDT1NYL3JlcG8tMS8uXy5EU19TdG9yZVVUDQAHRo/vZkqP72ZKj+9mdXgLAAEE9QEAAAQUAAAAY2AVY2dgYmDwTUxW8A9WiFCAApAYAycQGwGxGxCD+BVAzABT4SDAgAM4hoQEQZkVMF3oAABQSwcIC4jAODUAAAB4AAAAUEsDBBQACAAIAMOrNVkAAAAAAAAAAAMAAAAPACAAcmVwby0xL3Rlc3QudHh0VVQNAAc+j+9mPo/vZj6P72Z1eAsAAQT1AQAABBQAAABLLE4BAFBLBwhx95n4BQAAAAMAAABQSwMEFAAIAAgAw6s1WQAAAAAAAAAABAIAABoAIABfX01BQ09TWC9yZXBvLTEvLl90ZXN0LnR4dFVUDQAHPo/vZj6P72ZKj+9mdXgLAAEE9QEAAAQUAAAAY2AVY2dgYmDwTUxW8A9WiFCAApAYAycQGzEwMF4C0kA+EwsDUcAxJCQIwgLpYFQBMh6gKYGJ8zMwiCfn5+olFhTkpOqFpFaUuOYl56dk5qWDlTEaAwkBBgYphJqcxOKS0uLUlJTEklTlgGCIcYzOQMIDaBxCXW5icg5ErhtIVDIwmCPJpZYkAvUnWmX7uvgkJqXmxJsU5yWXllaZlKdnJ5mn5CQWmeYXFyZWmRWZADWXlqTpWlgbGpsYGZpbWpjY9L9PAxk85eoeUbANjRPWeOw4Nd8xf0Vn3PWb7LE5xAUTOvj06q8Za2WjjdYkd62c1VOfPK/gWeYWJldUMEtF4Mys13Z7kq7Vbmavkqpj7OEK+tP2ZJ1X9kvx16/+iLEcOV98W8ApLkPlkvi3uwsPqjd9u5uleU7uWkbWhp6S/8ukTj7cnrF27utNFW91X50/vD7+/YtZB3RmTFNmf6DBnXrBCQBQSwcIFmc02GQBAAAEAgAAUEsDBBQAAAAAAMCrNVkAAAAAAAAAAAAAAAAPACAAcmVwby0xL2ZvbGRlcjEvVVQNAAc5j+9mOY/vZjmP72Z1eAsAAQT1AQAABBQAAABQSwMEFAAIAAgAwKs1WQAAAAAAAAAAAQAAABgAIAByZXBvLTEvZm9sZGVyMS90ZXN0Mi50eHRVVA0ABzmP72Y6j+9mOY/vZnV4CwABBPUBAAAEFAAAAEsEAFBLBwhDvrfoAwAAAAEAAABQSwMEFAAIAAgAwKs1WQAAAAAAAAAABAIAACMAIABfX01BQ09TWC9yZXBvLTEvZm9sZGVyMS8uX3Rlc3QyLnR4dFVUDQAHOY/vZjqP72ZKj+9mdXgLAAEE9QEAAAQUAAAAY2AVY2dgYmDwTUxW8A9WiFCAApAYAycQGzEwMF4C0kA+EwsDUcAxJCQIwgLpYFQBMh6gKYGJ8zMwiCfn5+olFhTkpOqFpFaUuOYl56dk5qWDlTEaAwkBBgYphJqcxOKS0uLUlJTEklTlgGCIcYzOQMIDaBxCXW5icg5ErhtIVDIwmCPJpZYkAvUnWmX7uvgkJqXmxJsU5yWXllaZlKdnJ5mn5CQWmeYXFyZWmRWZADWXlqTpWlgbGpsYGZpbWpiY9b9PAxk8dTEvF9iGxglrPHacmu+Yv6Iz7vpN9tgc4oIJHXy6Zh6zQXZmDJvEvpTmI65pK99er03L9hbiyLPzKs2f9lvVgPdoTuqHU/mPvsn2rZBm6l9ryByVlxbrOul3XUzeq30mls81JIyvvr5rma87+eKlRW2lXumGV1yLQsKcUpZMe+bi9eyrmYzoZbuAgu4v5vs9Innjdy46fpvhJAMAUEsHCMeEj1liAQAABAIAAFBLAQIUAxQAAAAAAMOrNVkAAAAAAAAAAAAAAAAHACAAAAAAAAAAAADtQQAAAAByZXBvLTEvVVQNAAc+j+9mPo/vZj6P72Z1eAsAAQT1AQAABBQAAABQSwECFAMUAAgACADHqzVZfOAVgaoBAAAEGAAAEAAgAAAAAAAAAAAApIFFAAAAcmVwby0xLy5EU19TdG9yZVVUDQAHRo/vZiGP72ZGj+9mdXgLAAEE9QEAAAQUAAAAUEsBAhQDFAAIAAgAx6s1WQuIwDg1AAAAeAAAABsAIAAAAAAAAAAAAKSBTQIAAF9fTUFDT1NYL3JlcG8tMS8uXy5EU19TdG9yZVVUDQAHRo/vZkqP72ZKj+9mdXgLAAEE9QEAAAQUAAAAUEsBAhQDFAAIAAgAw6s1WXH3mfgFAAAAAwAAAA8AIAAAAAAAAAAAAKSB6wIAAHJlcG8tMS90ZXN0LnR4dFVUDQAHPo/vZj6P72Y+j+9mdXgLAAEE9QEAAAQUAAAAUEsBAhQDFAAIAAgAw6s1WRZnNNhkAQAABAIAABoAIAAAAAAAAAAAAKSBTQMAAF9fTUFDT1NYL3JlcG8tMS8uX3Rlc3QudHh0VVQNAAc+j+9mPo/vZkqP72Z1eAsAAQT1AQAABBQAAABQSwECFAMUAAAAAADAqzVZAAAAAAAAAAAAAAAADwAgAAAAAAAAAAAA7UEZBQAAcmVwby0xL2ZvbGRlcjEvVVQNAAc5j+9mOY/vZjmP72Z1eAsAAQT1AQAABBQAAABQSwECFAMUAAgACADAqzVZQ7636AMAAAABAAAAGAAgAAAAAAAAAAAApIFmBQAAcmVwby0xL2ZvbGRlcjEvdGVzdDIudHh0VVQNAAc5j+9mOo/vZjmP72Z1eAsAAQT1AQAABBQAAABQSwECFAMUAAgACADAqzVZx4SPWWIBAAAEAgAAIwAgAAAAAAAAAAAApIHPBQAAX19NQUNPU1gvcmVwby0xL2ZvbGRlcjEvLl90ZXN0Mi50eHRVVA0ABzmP72Y6j+9mSo/vZnV4CwABBPUBAAAEFAAAAFBLBQYAAAAACAAIABUDAACiBwAAAAA="  # noqa: E501


# repo-2
# └── somefile
# repo-3
# └── somefile
TEST_REPOS_2_3_ZIP = "UEsDBBQAAAAAAOerNVkAAAAAAAAAAAAAAAAHACAAcmVwby0zL1VUDQAHgo/vZoKP72aCj+9mdXgLAAEE9QEAAAQUAAAAUEsDBAoACAAAAOCrNVkAAAAAAAAAAAAAAAAPACAAcmVwby0zL3NvbWVmaWxlVVQNAAd0j+9mdI/vZoKP72Z1eAsAAQT1AQAABBQAAABQSwcIAAAAAAAAAAAAAAAAUEsDBBQAAAAAANmrNVkAAAAAAAAAAAAAAAAHACAAcmVwby0yL1VUDQAHa4/vZmuP72Zrj+9mdXgLAAEE9QEAAAQUAAAAUEsDBAoACAAAANmrNVkAAAAAAAAAAAAAAAAPACAAcmVwby0yL3NvbWVmaWxlVVQNAAdrj+9ma4/vZmuP72Z1eAsAAQT1AQAABBQAAABQSwcIAAAAAAAAAAAAAAAAUEsBAhQDFAAAAAAA56s1WQAAAAAAAAAAAAAAAAcAIAAAAAAAAAAAAO1BAAAAAHJlcG8tMy9VVA0AB4KP72aCj+9mgo/vZnV4CwABBPUBAAAEFAAAAFBLAQIKAwoACAAAAOCrNVkAAAAAAAAAAAAAAAAPACAAAAAAAAAAAACkgUUAAAByZXBvLTMvc29tZWZpbGVVVA0AB3SP72Z0j+9mgo/vZnV4CwABBPUBAAAEFAAAAFBLAQIUAxQAAAAAANmrNVkAAAAAAAAAAAAAAAAHACAAAAAAAAAAAADtQaIAAAByZXBvLTIvVVQNAAdrj+9ma4/vZmuP72Z1eAsAAQT1AQAABBQAAABQSwECCgMKAAgAAADZqzVZAAAAAAAAAAAAAAAADwAgAAAAAAAAAAAApIHnAAAAcmVwby0yL3NvbWVmaWxlVVQNAAdrj+9ma4/vZmuP72Z1eAsAAQT1AQAABBQAAABQSwUGAAAAAAQABABkAQAARAEAAAAA"  # noqa: E501

# repo-3
# └── somefile
TEST_REPO_3_ZIP = "UEsDBBQAAAAAAOerNVkAAAAAAAAAAAAAAAAHACAAcmVwby0zL1VUDQAHgo/vZoKP72aCj+9mdXgLAAEE9QEAAAQUAAAAUEsDBAoACAAAAOCrNVkAAAAAAAAAAAAAAAAPACAAcmVwby0zL3NvbWVmaWxlVVQNAAd0j+9mdI/vZoKP72Z1eAsAAQT1AQAABBQAAABQSwcIAAAAAAAAAAAAAAAAUEsBAhQDFAAAAAAA56s1WQAAAAAAAAAAAAAAAAcAIAAAAAAAAAAAAO1BAAAAAHJlcG8tMy9VVA0AB4KP72aCj+9mgo/vZnV4CwABBPUBAAAEFAAAAFBLAQIKAwoACAAAAOCrNVkAAAAAAAAAAAAAAAAPACAAAAAAAAAAAACkgUUAAAByZXBvLTMvc29tZWZpbGVVVA0AB3SP72Z0j+9mgo/vZnV4CwABBPUBAAAEFAAAAFBLBQYAAAAAAgACALIAAACiAAAAAAA="  # noqa: E501

TEST_REPO_3_EXTRA_ZIP = "UEsDBAoACAAAAAWuNVkAAAAAAAAAAAAAAAAIACAAc29tZWZpbGVVVA0AB3uT72Z7k+9me5PvZnV4CwABBPUBAAAEFAAAAFBLBwgAAAAAAAAAAAAAAABQSwMEFAAAAAAA56s1WQAAAAAAAAAAAAAAAAcAIAByZXBvLTMvVVQNAAeCj+9mgo/vZoKP72Z1eAsAAQT1AQAABBQAAABQSwMECgAIAAAA4Ks1WQAAAAAAAAAAAAAAAA8AIAByZXBvLTMvc29tZWZpbGVVVA0AB3SP72Z0j+9mgo/vZnV4CwABBPUBAAAEFAAAAFBLBwgAAAAAAAAAAAAAAABQSwECCgMKAAgAAAAFrjVZAAAAAAAAAAAAAAAACAAgAAAAAAAAAAAApIEAAAAAc29tZWZpbGVVVA0AB3uT72Z7k+9me5PvZnV4CwABBPUBAAAEFAAAAFBLAQIUAxQAAAAAAOerNVkAAAAAAAAAAAAAAAAHACAAAAAAAAAAAADtQVYAAAByZXBvLTMvVVQNAAeCj+9mgo/vZoKP72Z1eAsAAQT1AQAABBQAAABQSwECCgMKAAgAAADgqzVZAAAAAAAAAAAAAAAADwAgAAAAAAAAAAAApIGbAAAAcmVwby0zL3NvbWVmaWxlVVQNAAd0j+9mdI/vZoKP72Z1eAsAAQT1AQAABBQAAABQSwUGAAAAAAMAAwAIAQAA+AAAAAAA"  # noqa: E501


def test_upload_zipfile(
    client: TestClient, normal_user_token_headers: dict[str, str], db: Session
) -> None:
    # Get user first
    response = client.get(
        f"{settings.API_V1_STR}/users/me",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    user_id = response.json()["id"]

    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    project_id = content["id"]

    file_bytes = base64.b64decode(TEST_REPO_1_ZIP)
    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/zipfile",
        headers=normal_user_token_headers,
        files={"file": ("test1.zip", file_bytes, "application/octet-stream")},
        timeout=5,
    )

    # Checking the note itself first
    assert response.status_code == 202
    content = response.json()

    # Wait for the background task to finish
    sleep(3)

    # Then check the artifacts created
    response = client.get(
        f"{settings.API_V1_STR}/projects/{project_id}/artifacts",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["count"] == 1
    assert content["data"][0]["type"] == "SOURCE_CODE_FILE"
    assert content["data"][0]["priority"] == "NONE"
    assert content["data"][0]["descriptor"] == "test1.zip"
    artifact_1_id = content["data"][0]["id"]
    assert "note_ids" in content["data"][0]
    assert len(content["data"][0]["note_ids"]) == 2
    note_1_id = content["data"][0]["note_ids"][0]
    note_2_id = content["data"][0]["note_ids"][1]

    # Get the notes to check correctness
    response = client.get(
        f"{settings.API_V1_STR}/notes/{note_1_id}",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["contents"] == f"ZIPFILE_LOCATION:{project_id}/test1.zip"
    assert content["type"] == "METADATA_KV"
    assert content["author_id"] == user_id
    assert content["artifact_id"] == artifact_1_id

    response = client.get(
        f"{settings.API_V1_STR}/notes/{note_2_id}",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["contents"] == "ZIPFILE_REPO:repo-1"
    assert content["type"] == "METADATA_KV"
    assert content["author_id"] == user_id
    assert content["artifact_id"] == artifact_1_id

    # Get the files to check correctness
    response = client.get(
        f"{settings.API_V1_STR}/artifacts/{artifact_1_id}/file",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    data = response.json()
    assert data["filename"] == "test1.zip"
    assert re.match(
        r"http://(storage|localhost):9000/loci-notes-files/\d+/test1.zip.*",
        data["link"],
    )
    assert response.headers["Content-Type"] == "application/json"


def test_upload_multirepo_zipfile(
    client: TestClient, normal_user_token_headers: dict[str, str], db: Session
) -> None:
    # Get user first
    response = client.get(
        f"{settings.API_V1_STR}/users/me",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    user_id = response.json()["id"]

    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    project_id = content["id"]

    file_bytes = base64.b64decode(TEST_REPOS_2_3_ZIP)
    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/zipfile",
        headers=normal_user_token_headers,
        files={"file": ("test1.zip", file_bytes, "application/octet-stream")},
        timeout=5,
    )
    assert response.status_code == 202
    content = response.json()

    # Wait for the background task to finish
    sleep(3)

    # Then check the artifacts created
    response = client.get(
        f"{settings.API_V1_STR}/projects/{project_id}/artifacts",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["count"] == 1
    assert content["data"][0]["type"] == "SOURCE_CODE_FILE"
    assert content["data"][0]["priority"] == "NONE"
    assert content["data"][0]["descriptor"] == "test1.zip"
    artifact_1_id = content["data"][0]["id"]
    assert "note_ids" in content["data"][0]
    assert len(content["data"][0]["note_ids"]) == 3
    note_1_id = content["data"][0]["note_ids"][0]
    note_2_id = content["data"][0]["note_ids"][1]
    note_3_id = content["data"][0]["note_ids"][2]

    # Get the notes to check correctness
    response = client.get(
        f"{settings.API_V1_STR}/notes/{note_1_id}",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["contents"] == f"ZIPFILE_LOCATION:{project_id}/test1.zip"
    assert content["type"] == "METADATA_KV"
    assert content["author_id"] == user_id
    assert content["artifact_id"] == artifact_1_id

    repo2_found = False
    repo3_found = False

    response = client.get(
        f"{settings.API_V1_STR}/notes/{note_2_id}",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    if content["contents"] == "ZIPFILE_REPO:repo-2":
        repo2_found = True
    elif content["contents"] == "ZIPFILE_REPO:repo-3":
        repo3_found = True
    assert content["type"] == "METADATA_KV"
    assert content["author_id"] == user_id
    assert content["artifact_id"] == artifact_1_id

    response = client.get(
        f"{settings.API_V1_STR}/notes/{note_3_id}",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    if content["contents"] == "ZIPFILE_REPO:repo-2":
        repo2_found = True
    elif content["contents"] == "ZIPFILE_REPO:repo-3":
        repo3_found = True
    assert content["type"] == "METADATA_KV"
    assert content["author_id"] == user_id
    assert content["artifact_id"] == artifact_1_id

    assert repo2_found
    assert repo3_found


def test_upload_zipfile_validation_duplicate_zipfile(
    client: TestClient, normal_user_token_headers: dict[str, str], db: Session
) -> None:
    # Get user first
    response = client.get(
        f"{settings.API_V1_STR}/users/me", headers=normal_user_token_headers
    )
    assert response.status_code == 200

    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    project_id = content["id"]

    file_bytes = base64.b64decode(TEST_REPO_1_ZIP)
    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/zipfile",
        headers=normal_user_token_headers,
        files={"file": ("test.zip", file_bytes, "application/zip")},
        timeout=5,
    )
    assert response.status_code == 202

    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/zipfile",
        headers=normal_user_token_headers,
        files={"file": ("test.zip", file_bytes, "application/octet-stream")},
    )
    assert response.status_code == 400
    assert response.json()["detail"] == "File already uploaded."


def test_upload_zipfile_validation_invalid_zipfile(
    client: TestClient, normal_user_token_headers: dict[str, str], db: Session
) -> None:
    # Get user first
    response = client.get(
        f"{settings.API_V1_STR}/users/me", headers=normal_user_token_headers
    )
    assert response.status_code == 200

    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    project_id = content["id"]

    file_bytes = base64.b64decode(TEST_REPO_1_ZIP)
    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/zipfile",
        headers=normal_user_token_headers,
        files={"file": ("test.zip", file_bytes, "application/zip")},
        timeout=5,
    )
    assert response.status_code == 202

    file_bytes = bytes("not a zip file", "utf-8")
    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/zipfile",
        headers=normal_user_token_headers,
        files={"file": ("test2.zip", file_bytes, "application/octet-stream")},
    )
    assert response.status_code == 400
    assert response.json()["detail"] == "Zip file is invalid."


def test_upload_zipfile_validation_duplicate_repo(
    client: TestClient, normal_user_token_headers: dict[str, str], db: Session
) -> None:
    # Get user first
    response = client.get(
        f"{settings.API_V1_STR}/users/me", headers=normal_user_token_headers
    )
    assert response.status_code == 200

    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    project_id = content["id"]

    file_bytes = base64.b64decode(TEST_REPOS_2_3_ZIP)
    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/zipfile",
        headers=normal_user_token_headers,
        files={"file": ("test.zip", file_bytes, "application/zip")},
        timeout=5,
    )
    assert response.status_code == 202

    file_bytes = base64.b64decode(TEST_REPO_3_ZIP)
    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/zipfile",
        headers=normal_user_token_headers,
        files={"file": ("test2.zip", file_bytes, "application/octet-stream")},
    )
    assert response.status_code == 400
    assert (
        response.json()["detail"]
        == "This ZIP file contains the repo-3 repo which was already uploaded "
        "in another ZIP file. You cannot upload it again."
    )


def test_download_nonfile(
    client: TestClient, normal_user_token_headers: dict[str, str], db: Session
) -> None:
    # Get user first
    response = client.get(
        f"{settings.API_V1_STR}/users/me", headers=normal_user_token_headers
    )
    assert response.status_code == 200

    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    project_id = content["id"]

    data = {
        "artifact_descriptor": "string",
        "artifact_type": "SOURCE_CODE_LOCATION",
        "artifact_priority": "HIGH",
        "submission_tool": "string",
        "type": "COMMENT",
        "contents": "string",
    }
    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/note",
        headers=normal_user_token_headers,
        json=data,
    )
    # Checking the note itself first
    assert response.status_code == 201
    content = response.json()
    assert "id" in content
    artifact_id = content["artifact_id"]

    response = client.get(
        f"{settings.API_V1_STR}/artifacts/{artifact_id}/file",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 400
    assert response.json()["detail"] == "Artifact is not a file."


def test_upload_zipfile_validation_invalid_zipfile_extra(
    client: TestClient, normal_user_token_headers: dict[str, str], db: Session
) -> None:
    # Get user first
    response = client.get(
        f"{settings.API_V1_STR}/users/me", headers=normal_user_token_headers
    )
    assert response.status_code == 200

    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    project_id = content["id"]

    file_bytes = base64.b64decode(TEST_REPOS_2_3_ZIP)
    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/zipfile",
        headers=normal_user_token_headers,
        files={"file": ("test.zip", file_bytes, "application/zip")},
        timeout=5,
    )
    assert response.status_code == 202

    file_bytes = base64.b64decode(TEST_REPO_3_EXTRA_ZIP)
    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/zipfile",
        headers=normal_user_token_headers,
        files={"file": ("test2.zip", file_bytes, "application/octet-stream")},
    )
    assert response.status_code == 400
    assert (
        response.json()["detail"] == "All files in the zipfile must be in a directory."
    )
