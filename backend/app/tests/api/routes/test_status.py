from fastapi.testclient import TestClient

from app.core.config import settings
from app.version import __version__


def test_get_status(
    client: TestClient,
) -> None:
    r = client.get(f"{settings.API_V1_STR}/status")
    assert r.status_code == 200
    data = r.json()
    assert data["version"] == __version__
    assert data["env"] == "local"
    assert "open_reg" in data
    assert "now" in data
    prev_time = data["now"]

    r = client.get(f"{settings.API_V1_STR}/status")
    assert r.status_code == 200
    data = r.json()
    assert "now" in data
    current_time = data["now"]

    assert prev_time != current_time
    assert prev_time < current_time
