import random

from fastapi.testclient import TestClient
from sqlmodel import Session

from app.core.config import settings


def test_create_artifact_by_descriptor(
    client: TestClient, normal_user_token_headers: dict[str, str], db: Session
) -> None:
    # Get user first
    response = client.get(
        f"{settings.API_V1_STR}/users/me", headers=normal_user_token_headers
    )
    assert response.status_code == 200
    user_id = response.json()["id"]

    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    project_id = content["id"]

    data = {
        "artifact_descriptor": "string",
        "artifact_type": "SOURCE_CODE_LOCATION",
        "artifact_priority": "HIGH",
        "submission_tool": "string",
        "type": "COMMENT",
        "contents": "string",
    }
    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/note",
        headers=normal_user_token_headers,
        json=data,
    )

    # Checking the note itself first
    assert response.status_code == 201
    content = response.json()
    assert "id" in content
    assert "artifact_id" in content
    artifact_id = content["artifact_id"]
    assert content["submission_tool"] == data["submission_tool"]
    assert content["type"] == data["type"]
    assert content["contents"] == data["contents"]
    assert content["author_id"] == user_id
    assert "created_at" in content
    assert "updated_at" in content

    # Then check the artifact created
    response = client.get(
        f"{settings.API_V1_STR}/artifacts/{artifact_id}",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["descriptor"] == data["artifact_descriptor"]
    assert content["priority"] == data["artifact_priority"]
    assert content["type"] == data["artifact_type"]

    data = {
        "artifact_descriptor": "string",
        "artifact_type": "SOURCE_CODE_LOCATION",
        "artifact_priority": "LOW",
        "type": "COMMENT",
        "contents": "string",
    }
    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/note",
        headers=normal_user_token_headers,
        json=data,
    )

    # Checking the note itself first
    assert response.status_code == 201
    content = response.json()
    assert "id" in content
    assert "artifact_id" in content
    assert content["artifact_id"] == artifact_id
    assert content["type"] == data["type"]
    assert content["contents"] == data["contents"]
    assert content["author_id"] == user_id
    assert "created_at" in content
    assert "updated_at" in content

    # Then check the artifact created
    response = client.get(
        f"{settings.API_V1_STR}/artifacts/{artifact_id}",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["priority"] == "LOW"

    # Check to make sure that a new note created with no explicit priority is
    # given a default priority of MEDIUM.
    data = {
        "artifact_descriptor": "string123",
        "artifact_type": "SOURCE_CODE_LOCATION",
        "type": "COMMENT",
        "contents": "string",
    }

    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/note",
        headers=normal_user_token_headers,
        json=data,
    )

    # Checking the note itself first
    assert response.status_code == 201
    content = response.json()
    assert "id" in content
    assert "artifact_id" in content
    artifact_id = content["artifact_id"]

    # Then check the artifact created
    response = client.get(
        f"{settings.API_V1_STR}/artifacts/{artifact_id}",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["priority"] == "MEDIUM"


def test_create_artifact_by_descriptor_duplicate(
    client: TestClient, normal_user_token_headers: dict[str, str], db: Session
) -> None:
    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    project_id = content["id"]

    data = {
        "artifact_descriptor": "string",
        "artifact_type": "SOURCE_CODE_LOCATION",
        "artifact_priority": "HIGH",
        "submission_tool": "string",
        "type": "COMMENT",
        "contents": "string",
    }
    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/note",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    artifact_id = content["artifact_id"]

    data = {
        "artifact_descriptor": "string",
        "artifact_type": "OTHER",
        "artifact_priority": "LOW",
        "submission_tool": "another tool",
        "type": "COMMENT",
        "contents": "some other contents",
    }
    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/note",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    assert content["artifact_id"] == artifact_id


def test_create_artifact_by_id(
    client: TestClient, normal_user_token_headers: dict[str, str], db: Session
) -> None:
    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    project_id = content["id"]

    data = {
        "artifact_descriptor": "string",
        "artifact_type": "SOURCE_CODE_LOCATION",
        "artifact_priority": "HIGH",
        "submission_tool": "string",
        "type": "COMMENT",
        "contents": "string",
    }
    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/note",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    artifact_id = content["artifact_id"]

    # An interesting side effect of using SQLmodel (or more likely Pydantic) is
    # that we can pass an int either as a JSON number, or as a string. We test
    # both below.
    data = {
        "artifact_id": artifact_id,
        "artifact_type": "OTHER",
        "artifact_priority": "LOW",
        "submission_tool": "another tool",
        "type": "COMMENT",
        "contents": "some other contents",
    }
    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/note",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    assert content["artifact_id"] == artifact_id

    data = {
        "artifact_id": int(artifact_id),  # type: ignore[dict-item]
        "artifact_type": "OTHER",
        "artifact_priority": "LOW",
        "submission_tool": "another tool",
        "type": "COMMENT",
        "contents": "some other contents",
    }
    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/note",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    assert content["artifact_id"] == artifact_id


def test_get_artifacts(
    client: TestClient, normal_user_token_headers: dict[str, str], db: Session
) -> None:
    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    project_id = content["id"]

    data = {
        "artifact_descriptor": "string",
        "artifact_type": "SOURCE_CODE_LOCATION",
        "artifact_priority": "HIGH",
        "submission_tool": "string",
        "type": "COMMENT",
        "contents": "string",
    }
    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/note",
        headers=normal_user_token_headers,
        json=data,
    )
    # Checking the note itself first
    assert response.status_code == 201
    content = response.json()
    assert "id" in content
    assert "artifact_id" in content
    artifact_id = content["artifact_id"]
    note_id = content["id"]

    # Then check the artifact created
    response = client.get(
        f"{settings.API_V1_STR}/artifacts/{artifact_id}",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["descriptor"] == data["artifact_descriptor"]
    assert content["priority"] == data["artifact_priority"]
    assert content["type"] == data["artifact_type"]
    assert "note_ids" in content
    assert len(content["note_ids"]) == 1
    assert content["note_ids"][0] == note_id

    response = client.get(
        f"{settings.API_V1_STR}/projects/{project_id}/artifacts",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert "data" in content
    assert len(content["data"]) == 1
    assert content["data"][0]["id"] == artifact_id
    assert content["count"] == 1
    assert content["data"][0]["descriptor"] == data["artifact_descriptor"]
    assert content["data"][0]["priority"] == data["artifact_priority"]
    assert content["data"][0]["type"] == data["artifact_type"]

    # Get the current time for later.
    response = client.get(
        f"{settings.API_V1_STR}/status",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    before_artifacts_created_time = content["now"]

    # Test the pagination
    # Make a bunch of new artifacts first
    for _ in range(20):
        data = {
            "artifact_descriptor": "string" + str(random.randint(0, 1000000)),
            "artifact_type": "SOURCE_CODE_LOCATION",
            "artifact_priority": "HIGH",
            "submission_tool": "string",
            "type": "COMMENT",
            "contents": "string",
        }
        response = client.post(
            f"{settings.API_V1_STR}/projects/{project_id}/note",
            headers=normal_user_token_headers,
            json=data,
        )
        # Checking the note itself first
        assert response.status_code == 201

    response = client.get(
        f"{settings.API_V1_STR}/projects/{project_id}/artifacts?limit=4",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert "data" in content
    assert len(content["data"]) == 4
    assert content["count"] == 21

    response = client.get(
        f"{settings.API_V1_STR}/projects/{project_id}/artifacts?limit=20&skip=20",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert "data" in content
    assert len(content["data"]) == 1
    assert content["count"] == 21

    response = client.get(
        f"{settings.API_V1_STR}/projects/{project_id}/artifacts"
        "?limit=999&sort=created_at&order=asc",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert "data" in content
    assert content["count"] == 21
    last_created_at = ""
    for artifact in content["data"]:
        if last_created_at:
            assert artifact["created_at"] >= last_created_at
        last_created_at = artifact["created_at"]

    response = client.get(
        f"{settings.API_V1_STR}/projects/{project_id}/artifacts"
        "?limit=999&sort=created_at&order=desc",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert "data" in content
    assert content["count"] == 21
    last_created_at = ""
    for artifact in content["data"]:
        if last_created_at:
            assert artifact["created_at"] <= last_created_at
        last_created_at = artifact["created_at"]

    response = client.get(
        f"{settings.API_V1_STR}/projects/{project_id}/artifacts"
        "?limit=10&sort=descriptor&order=desc",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert "data" in content
    assert content["count"] == 21
    last_descriptor = ""
    for artifact in content["data"]:
        if last_descriptor:
            assert artifact["descriptor"] <= last_descriptor
        last_descriptor = artifact["descriptor"]

    response = client.get(
        f"{settings.API_V1_STR}/projects/{project_id}/artifacts?updated_before="
        + str(before_artifacts_created_time),
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert "data" in content
    assert content["count"] == 1
    assert content["data"][0]["id"] == artifact_id

    response = client.get(
        f"{settings.API_V1_STR}/projects/{project_id}/artifacts?updated_after="
        + str(before_artifacts_created_time),
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert "data" in content
    assert content["count"] == 20

    # Get the current time for later.
    response = client.get(
        f"{settings.API_V1_STR}/status",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    after_artifacts_created_time = content["now"]

    response = client.get(
        f"{settings.API_V1_STR}/projects/{project_id}/artifacts?updated_after="
        + str(after_artifacts_created_time)
        + "&updated_before="
        + str(before_artifacts_created_time),
        headers=normal_user_token_headers,
    )
    assert response.status_code == 400
    assert response.json()["detail"] == "updated_after must be before updated_before"

    response = client.get(
        f"{settings.API_V1_STR}/projects/{project_id}/artifacts?updated_after="
        + str(before_artifacts_created_time)
        + "&updated_before="
        + str(after_artifacts_created_time),
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert "data" in content
    assert content["count"] == 20


def test_update_artifact(
    client: TestClient, normal_user_token_headers: dict[str, str], db: Session
) -> None:
    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    project_id = content["id"]

    data = {
        "artifact_descriptor": "string",
        "artifact_type": "SOURCE_CODE_LOCATION",
        "artifact_priority": "HIGH",
        "submission_tool": "string",
        "type": "COMMENT",
        "contents": "string",
    }
    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/note",
        headers=normal_user_token_headers,
        json=data,
    )
    # Checking the note itself first
    assert response.status_code == 201
    content = response.json()
    assert "id" in content
    assert "artifact_id" in content
    artifact_id = content["artifact_id"]

    # Then check the artifact created
    response = client.get(
        f"{settings.API_V1_STR}/artifacts/{artifact_id}",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["descriptor"] == data["artifact_descriptor"]
    assert content["priority"] == data["artifact_priority"]
    assert content["type"] == data["artifact_type"]
    artifact_id = content["id"]

    data2 = {
        "priority": "LOW",
        "type": "SOURCE_CODE_LOCATION",
    }
    response = client.put(
        f"{settings.API_V1_STR}/artifacts/{artifact_id}",
        headers=normal_user_token_headers,
        json=data2,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["descriptor"] == data["artifact_descriptor"]
    assert content["id"] == artifact_id
    assert content["priority"] == data2["priority"]
    assert content["type"] == data2["type"]


def test_delete_artifact(
    client: TestClient, normal_user_token_headers: dict[str, str], db: Session
) -> None:
    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    project_id = content["id"]

    data = {
        "artifact_descriptor": "string",
        "artifact_type": "SOURCE_CODE_LOCATION",
        "artifact_priority": "HIGH",
        "submission_tool": "string",
        "type": "COMMENT",
        "contents": "string",
    }
    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/note",
        headers=normal_user_token_headers,
        json=data,
    )
    # Checking the note itself first
    assert response.status_code == 201
    content = response.json()
    assert "id" in content
    assert "artifact_id" in content
    artifact_id = content["artifact_id"]
    note_id = content["id"]

    response = client.delete(
        f"{settings.API_V1_STR}/artifacts/{artifact_id}",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["message"] == "Artifact deleted"

    # Make sure we get back 404s on the artifact and note we deleted.
    response = client.get(
        f"{settings.API_V1_STR}/artifacts/{artifact_id}",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 404
    assert response.json()["detail"] == "Artifact not found"

    response = client.get(
        f"{settings.API_V1_STR}/notes/{note_id}",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 404
    assert response.json()["detail"] == "Note not found"


def test_get_note(
    client: TestClient, normal_user_token_headers: dict[str, str], db: Session
) -> None:
    # Get the current user
    response = client.get(
        f"{settings.API_V1_STR}/users/me", headers=normal_user_token_headers
    )
    assert response.status_code == 200
    user_id = response.json()["id"]

    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    project_id = content["id"]

    data = {
        "artifact_descriptor": "string",
        "artifact_type": "SOURCE_CODE_LOCATION",
        "artifact_priority": "HIGH",
        "submission_tool": "string",
        "type": "COMMENT",
        "contents": "string",
    }
    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/note",
        headers=normal_user_token_headers,
        json=data,
    )
    # Checking the note itself first
    assert response.status_code == 201
    content = response.json()
    assert "id" in content
    note_id = content["id"]

    response = client.get(
        f"{settings.API_V1_STR}/notes/{note_id}",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["id"] == note_id
    assert content["artifact_id"] == content["artifact_id"]
    assert content["submission_tool"] == data["submission_tool"]
    assert content["type"] == data["type"]
    assert content["contents"] == data["contents"]
    assert content["author_id"] == user_id
    assert "created_at" in content
    assert "updated_at" in content


def test_update_note(
    client: TestClient, normal_user_token_headers: dict[str, str], db: Session
) -> None:
    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    project_id = content["id"]

    data = {
        "artifact_descriptor": "string",
        "artifact_type": "SOURCE_CODE_LOCATION",
        "artifact_priority": "HIGH",
        "submission_tool": "string",
        "type": "COMMENT",
        "contents": "string",
    }
    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/note",
        headers=normal_user_token_headers,
        json=data,
    )
    # Checking the note itself first
    assert response.status_code == 201
    content = response.json()
    assert "id" in content
    note_id = content["id"]
    original_content = content

    data2 = {
        "contents": "new contents",
    }
    response = client.put(
        f"{settings.API_V1_STR}/notes/{note_id}",
        headers=normal_user_token_headers,
        json=data2,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["contents"] == data2["contents"]
    assert content["id"] == note_id
    assert content["artifact_id"] == original_content["artifact_id"]
    assert content["submission_tool"] == original_content["submission_tool"]
    assert content["type"] == original_content["type"]
    assert content["author_id"] == original_content["author_id"]
    assert content["created_at"] == original_content["created_at"]
    assert content["updated_at"] != original_content["updated_at"]
    note_content = content

    # Last make sure the artifact shows that it was updated
    response = client.get(
        f"{settings.API_V1_STR}/artifacts/{original_content['artifact_id']}",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["updated_at"] == note_content["updated_at"]


def test_delete_note(
    client: TestClient, normal_user_token_headers: dict[str, str], db: Session
) -> None:
    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    project_id = content["id"]

    data = {
        "artifact_descriptor": "string",
        "artifact_type": "SOURCE_CODE_LOCATION",
        "artifact_priority": "HIGH",
        "submission_tool": "string",
        "type": "COMMENT",
        "contents": "string",
    }
    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/note",
        headers=normal_user_token_headers,
        json=data,
    )
    # Checking the note itself first
    assert response.status_code == 201
    content = response.json()
    assert "id" in content
    note_id = content["id"]
    artifact_id = content["artifact_id"]

    # Grab the artifact information for later
    response = client.get(
        f"{settings.API_V1_STR}/artifacts/{artifact_id}",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    original_artifact_content = response.json()
    assert original_artifact_content["updated_at"]

    response = client.delete(
        f"{settings.API_V1_STR}/notes/{note_id}",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["message"] == "Note deleted"

    # Make sure the note is deleted, but the artifact still exists
    response = client.get(
        f"{settings.API_V1_STR}/notes/{note_id}",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 404
    assert response.json()["detail"] == "Note not found"

    response = client.get(
        f"{settings.API_V1_STR}/artifacts/{artifact_id}",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200

    # Make sure the note is deleted, but the artifact still exists, and shows that
    # it was updated.
    response = client.get(
        f"{settings.API_V1_STR}/artifacts/{artifact_id}",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["updated_at"] != original_artifact_content["updated_at"]


def test_create_note_double_submission(
    client: TestClient, normal_user_token_headers: dict[str, str], db: Session
) -> None:
    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    project_id = content["id"]

    data = {
        "artifact_descriptor": "string",
        "artifact_type": "SOURCE_CODE_LOCATION",
        "artifact_priority": "HIGH",
        "submission_tool": "string",
        "type": "COMMENT",
        "contents": "string",
    }
    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/note",
        headers=normal_user_token_headers,
        json=data,
    )
    # Checking the note itself first
    assert response.status_code == 201
    content = response.json()
    assert "id" in content
    note_id = content["id"]

    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/note",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    assert content["id"] == note_id


def test_create_note_no_artifact(
    client: TestClient, normal_user_token_headers: dict[str, str], db: Session
) -> None:
    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    project_id = content["id"]

    data = {
        "artifact_type": "SOURCE_CODE_LOCATION",
        "artifact_priority": "HIGH",
        "submission_tool": "string",
        "type": "COMMENT",
        "contents": "string",
    }
    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/note",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 400
    assert (
        response.json()["detail"]
        == "Either artifact_id or artifact_descriptor must be specified."
    )


def test_create_note_bad_artifact_id(
    client: TestClient, normal_user_token_headers: dict[str, str], db: Session
) -> None:
    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    project_id = content["id"]

    data = {
        "artifact_id": 999999,  # type: ignore[dict-item]
        "artifact_type": "SOURCE_CODE_LOCATION",
        "artifact_priority": "HIGH",
        "submission_tool": "string",
        "type": "COMMENT",
        "contents": "string",
    }
    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/note",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 404
    assert response.json()["detail"] == "Artifact not found"


def test_artifact_access(
    client: TestClient,
    normal_user_token_headers: dict[str, str],
    superuser_token_headers: dict[str, str],
    db: Session,
) -> None:
    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=superuser_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    project_id = content["id"]

    data = {
        "artifact_descriptor": "string",
        "artifact_type": "SOURCE_CODE_LOCATION",
        "artifact_priority": "HIGH",
        "submission_tool": "string",
        "type": "COMMENT",
        "contents": "string",
    }
    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/note",
        headers=superuser_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    assert "id" in content
    note_id = content["id"]

    response = client.get(
        f"{settings.API_V1_STR}/notes/{note_id}",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 403
    assert "You do not have access to" in response.json()["detail"]


def test_get_artifacts_filters(
    client: TestClient, normal_user_token_headers: dict[str, str], db: Session
) -> None:
    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    project_id = content["id"]

    # Make a bunch of new artifacts first. Technically this can fail, but I'll
    # make enough so that we have a nice spread of everything, and getting all
    # of one type for any artifact is basically impossible.
    for i in range(100):
        data = {
            "artifact_descriptor": "string " + str(i),
            "artifact_type": random.choice(["SOURCE_CODE_LOCATION", "OTHER"]),
            "artifact_priority": random.choice(["HIGH", "MEDIUM", "LOW", "NONE"]),
            "submission_tool": "string",
            "type": "COMMENT",
            "contents": "string",
        }
        response = client.post(
            f"{settings.API_V1_STR}/projects/{project_id}/note",
            headers=normal_user_token_headers,
            json=data,
        )
        # Checking the note itself first
        assert response.status_code == 201

    response = client.get(
        f"{settings.API_V1_STR}/projects/{project_id}/"
        "artifacts?limit=4&filter=string%201",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["count"] == 11

    response = client.get(
        f"{settings.API_V1_STR}/projects/{project_id}/artifacts"
        "?limit=4&type=SOURCE_CODE_LOCATION",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["count"] < 100 and content["count"] > 0

    response = client.get(
        f"{settings.API_V1_STR}/projects/{project_id}/artifacts"
        "?limit=4&priority=NONE",  # noqa: E501
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["count"] < 100 and content["count"] > 0

    response = client.get(
        f"{settings.API_V1_STR}/projects/{project_id}/artifacts"
        "?limit=4&priority=LOW",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["count"] < 100 and content["count"] > 0


# This will make sure that a bug, where a new comment to an artifact that
# already exists resets the priority, etc. of the artifact, is fixed.
def test_create_artifact_ensure_no_changes(
    client: TestClient, normal_user_token_headers: dict[str, str], db: Session
) -> None:
    # Get user first
    response = client.get(
        f"{settings.API_V1_STR}/users/me", headers=normal_user_token_headers
    )
    assert response.status_code == 200
    user_id = response.json()["id"]

    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    project_id = content["id"]

    data = {
        "artifact_descriptor": "string",
        "submission_tool": "string",
        "type": "COMMENT",
        "contents": "string",
    }
    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/note",
        headers=normal_user_token_headers,
        json=data,
    )

    # Checking the note itself first
    assert response.status_code == 201
    content = response.json()
    assert "id" in content
    assert "artifact_id" in content
    artifact_id = content["artifact_id"]
    assert content["submission_tool"] == data["submission_tool"]
    assert content["type"] == data["type"]
    assert content["contents"] == data["contents"]
    assert content["author_id"] == user_id
    assert "created_at" in content
    assert "updated_at" in content

    # Then check the artifact created
    response = client.get(
        f"{settings.API_V1_STR}/artifacts/{artifact_id}",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["descriptor"] == data["artifact_descriptor"]
    assert content["priority"] == "MEDIUM"
    assert content["type"] == "OTHER"

    # Change the artifact
    data2 = {
        "priority": "LOW",
        "type": "SOURCE_CODE_LOCATION",
    }
    response = client.put(
        f"{settings.API_V1_STR}/artifacts/{artifact_id}",
        headers=normal_user_token_headers,
        json=data2,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["descriptor"] == data["artifact_descriptor"]
    assert content["id"] == artifact_id
    assert content["priority"] == data2["priority"]
    assert content["type"] == data2["type"]

    # Get the artifact again to make sure it didn't change back to defaults.

    response = client.get(
        f"{settings.API_V1_STR}/artifacts/{artifact_id}",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["descriptor"] == data["artifact_descriptor"]
    assert content["priority"] == data2["priority"]
    assert content["type"] == data2["type"]
