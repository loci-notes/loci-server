from fastapi.testclient import TestClient
from sqlmodel import Session

from app.core.config import settings
from app.models import UserOut


def test_create_project_as_user(
    client: TestClient, normal_user_token_headers: dict[str, str], db: Session
) -> None:
    # Get user first
    response = client.get(
        f"{settings.API_V1_STR}/users/me", headers=normal_user_token_headers
    )
    assert response.status_code == 200
    user = UserOut.model_validate(response.json())

    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    assert content["name"] == data["name"]
    assert "id" in content
    assert "created_at" in content
    for proj_user in content["users"]:
        assert proj_user["id"] == user.id
        assert UserOut.model_validate(proj_user)
    for proj_manager in content["managers"]:
        assert proj_manager["id"] == user.id
        assert UserOut.model_validate(proj_manager)


def test_get_project_list_as_user(
    client: TestClient, normal_user_token_headers: dict[str, str], db: Session
) -> None:
    response = client.get(
        f"{settings.API_V1_STR}/projects", headers=normal_user_token_headers
    )
    assert response.status_code == 200
    content = response.json()
    assert "count" in content
    assert "data" in content
    assert len(content["data"]) >= 0
    for proj in content["data"]:
        assert "id" in proj
        assert "name" in proj
    number_of_projects = content["count"]

    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201

    response = client.get(
        f"{settings.API_V1_STR}/projects", headers=normal_user_token_headers
    )
    assert response.status_code == 200
    content = response.json()
    assert content["count"] == number_of_projects + 1
    assert len(content["data"]) == number_of_projects + 1


def test_get_project_list_pagination(
    client: TestClient, normal_user_token_headers: dict[str, str], db: Session
) -> None:
    response = client.get(
        f"{settings.API_V1_STR}/projects", headers=normal_user_token_headers
    )
    assert response.status_code == 200
    content = response.json()
    assert "data" in content
    assert len(content["data"]) >= 0
    assert "count" in content
    assert content["count"] == len(content["data"])
    number_of_projects = content["count"]

    for i in range(10):
        data = {"name": "Test Project " + str(i)}
        response = client.post(
            f"{settings.API_V1_STR}/projects",
            headers=normal_user_token_headers,
            json=data,
        )
        assert response.status_code == 201

    response = client.get(
        f"{settings.API_V1_STR}/projects", headers=normal_user_token_headers
    )
    assert response.status_code == 200
    content = response.json()
    assert len(content["data"]) == number_of_projects + 10

    response = client.get(
        f"{settings.API_V1_STR}/projects?skip=0&limit=6",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert len(content["data"]) == 6
    assert content["count"] == number_of_projects + 10

    response = client.get(
        f"{settings.API_V1_STR}/projects?skip=0&limit=9999",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert len(content["data"]) == number_of_projects + 10
    assert content["count"] == number_of_projects + 10

    response = client.get(
        f"{settings.API_V1_STR}/projects?limit=9999&sort=name&order=asc",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    last_project_name = ""
    for proj in content["data"]:
        if last_project_name:
            assert proj["name"] >= last_project_name
        last_project_name = proj["name"]

    response = client.get(
        f"{settings.API_V1_STR}/projects?limit=9999&sort=name&order=desc",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    last_project_name = ""
    for proj in content["data"]:
        if last_project_name:
            assert proj["name"] <= last_project_name
        last_project_name = proj["name"]

    response = client.get(
        f"{settings.API_V1_STR}/projects?limit=9999&sort=created_at&order=asc",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    last_project_created_at = ""
    for proj in content["data"]:
        if last_project_created_at:
            assert proj["created_at"] >= last_project_created_at
        last_project_created_at = proj["created_at"]


def test_get_project_list_as_user_with_superuser_project(
    client: TestClient,
    normal_user_token_headers: dict[str, str],
    superuser_token_headers: dict[str, str],
    db: Session,
) -> None:
    response = client.get(
        f"{settings.API_V1_STR}/projects", headers=normal_user_token_headers
    )
    assert response.status_code == 200
    content = response.json()
    assert "count" in content
    assert "data" in content
    assert len(content["data"]) == content["count"]
    for proj in content["data"]:
        assert "id" in proj
        assert "name" in proj

    number_of_projects = content["count"]

    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=superuser_token_headers,
        json=data,
    )
    assert response.status_code == 201

    response = client.get(
        f"{settings.API_V1_STR}/projects", headers=normal_user_token_headers
    )
    assert response.status_code == 200
    content = response.json()
    assert content["count"] == number_of_projects


def test_get_project_list_as_superuser_with_user_project(
    client: TestClient,
    normal_user_token_headers: dict[str, str],
    superuser_token_headers: dict[str, str],
    db: Session,
) -> None:
    response = client.get(
        f"{settings.API_V1_STR}/projects", headers=superuser_token_headers
    )
    assert response.status_code == 200
    content = response.json()
    assert "count" in content
    assert "data" in content
    assert len(content["data"]) == content["count"]
    number_of_projects = content["count"]

    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    new_project_id = response.json()["id"]

    response = client.get(
        f"{settings.API_V1_STR}/projects", headers=superuser_token_headers
    )
    assert response.status_code == 200
    content = response.json()
    assert content["count"] == number_of_projects + 1
    found = False
    for proj in content["data"]:
        if proj["id"] == new_project_id:
            found = True
    assert found


def test_get_user_info_with_projects(
    client: TestClient, normal_user_token_headers: dict[str, str], db: Session
) -> None:
    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    project_id = response.json()["id"]

    response = client.get(
        f"{settings.API_V1_STR}/users/me", headers=normal_user_token_headers
    )
    assert response.status_code == 200
    content = response.json()
    assert "id" in content
    assert "email" in content
    assert "projects" in content
    found = False
    for proj in content["projects"]:
        assert "id" in proj
        assert "name" in proj
        if proj["id"] == project_id:
            found = True
    assert found

    found = False
    assert "managed_projects" in content
    for proj in content["managed_projects"]:
        assert "id" in proj
        assert "name" in proj
        if proj["id"] == project_id:
            found = True
    assert found


def test_read_project(
    client: TestClient,
    normal_user_token_headers: dict[str, str],
    db: Session,
) -> None:
    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    project_id = response.json()["id"]

    response = client.get(
        f"{settings.API_V1_STR}/projects/{project_id}",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert "id" in content
    assert "name" in content
    assert "created_at" in content
    assert content["id"] == project_id
    assert content["name"] == data["name"]
    assert "users" in content
    assert "managers" in content


def test_read_project_noexist(
    client: TestClient,
    normal_user_token_headers: dict[str, str],
    db: Session,
) -> None:
    response = client.get(
        f"{settings.API_V1_STR}/projects/99999999",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 404
    content = response.json()
    assert "detail" in content
    assert content["detail"] == "Project not found"


def test_create_project_no_default_access(
    client: TestClient,
    superuser_token_headers: dict[str, str],
    normal_user_token_headers: dict[str, str],
    db: Session,
) -> None:
    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=superuser_token_headers,
        json=data,
    )
    assert response.status_code == 201
    project_id = response.json()["id"]

    response = client.get(
        f"{settings.API_V1_STR}/projects/{project_id}", headers=superuser_token_headers
    )
    assert response.status_code == 200

    response = client.get(
        f"{settings.API_V1_STR}/projects/{project_id}",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 403
    assert "You do not have access to project" in response.json()["detail"]


def test_update_project(
    client: TestClient,
    normal_user_token_headers: dict[str, str],
    db: Session,
) -> None:
    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    project_id = content["id"]
    assert content["name"] == data["name"]

    response = client.get(
        f"{settings.API_V1_STR}/projects/{project_id}",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200

    data2 = {"name": "Test Project 2"}
    response = client.put(
        f"{settings.API_V1_STR}/projects/{project_id}",
        headers=normal_user_token_headers,
        json=data2,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["name"] == data2["name"]


def test_delete_project(
    client: TestClient,
    normal_user_token_headers: dict[str, str],
    db: Session,
) -> None:
    response = client.get(
        f"{settings.API_V1_STR}/projects", headers=normal_user_token_headers
    )
    assert response.status_code == 200
    content = response.json()
    assert len(content) >= 0
    number_of_projects = len(content["data"])

    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    project_id = content["id"]
    assert content["name"] == data["name"]

    response = client.get(
        f"{settings.API_V1_STR}/projects", headers=normal_user_token_headers
    )
    assert response.status_code == 200
    content = response.json()
    assert len(content["data"]) == number_of_projects + 1
    assert content["count"] == number_of_projects + 1

    response = client.delete(
        f"{settings.API_V1_STR}/projects/{project_id}",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["message"] == "Project deleted"

    response = client.get(
        f"{settings.API_V1_STR}/projects", headers=normal_user_token_headers
    )
    assert response.status_code == 200
    content = response.json()
    assert len(content["data"]) == number_of_projects


def test_project_add_users_as_manager(
    client: TestClient,
    superuser_token_headers: dict[str, str],
    normal_user_token_headers: dict[str, str],
    db: Session,
) -> None:
    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=superuser_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    project_id = content["id"]

    response = client.get(
        f"{settings.API_V1_STR}/users/me", headers=normal_user_token_headers
    )
    assert response.status_code == 200
    user_id = response.json()["id"]

    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/add_users",
        headers=superuser_token_headers,
        json=[{"id": user_id}],
    )
    assert response.status_code == 200
    content = response.json()
    assert content["id"] == project_id
    assert len(content["users"]) == 2
    assert len(content["managers"]) == 1
    found = False
    for proj_user in content["users"]:
        if proj_user["id"] == user_id:
            found = True
    assert found

    response = client.get(
        f"{settings.API_V1_STR}/users/me", headers=normal_user_token_headers
    )
    assert response.status_code == 200
    found = False
    for proj in response.json()["projects"]:
        if proj["id"] == project_id:
            found = True
    assert found


def test_project_add_managers_as_manager(
    client: TestClient,
    superuser_token_headers: dict[str, str],
    normal_user_token_headers: dict[str, str],
    db: Session,
) -> None:
    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=superuser_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    project_id = content["id"]

    response = client.get(
        f"{settings.API_V1_STR}/users/me", headers=normal_user_token_headers
    )
    assert response.status_code == 200
    user_id = response.json()["id"]

    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/add_users",
        headers=superuser_token_headers,
        json=[{"id": user_id, "is_manager": True}],
    )
    assert response.status_code == 200
    content = response.json()
    assert content["id"] == project_id
    assert len(content["users"]) == 2
    assert len(content["managers"]) == 2
    found = False
    for proj_user in content["users"]:
        if proj_user["id"] == user_id:
            found = True
    assert found

    found = False
    for proj_user in content["managers"]:
        if proj_user["id"] == user_id:
            found = True
    assert found

    response = client.get(
        f"{settings.API_V1_STR}/users/me", headers=normal_user_token_headers
    )
    assert response.status_code == 200
    found = False
    for proj in response.json()["projects"]:
        if proj["id"] == project_id:
            found = True
    assert found

    found = False
    for proj in response.json()["managed_projects"]:
        if proj["id"] == project_id:
            found = True
    assert found


def test_project_remove_users_as_manager(
    client: TestClient,
    superuser_token_headers: dict[str, str],
    normal_user_token_headers: dict[str, str],
    db: Session,
) -> None:
    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=superuser_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    project_id = content["id"]

    response = client.get(
        f"{settings.API_V1_STR}/users/me", headers=normal_user_token_headers
    )
    assert response.status_code == 200
    user_id = response.json()["id"]

    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/add_users",
        headers=superuser_token_headers,
        json=[{"id": user_id}],
    )
    assert response.status_code == 200
    content = response.json()
    assert content["id"] == project_id
    assert len(content["users"]) == 2
    assert len(content["managers"]) == 1
    found = False
    for proj_user in content["users"]:
        if proj_user["id"] == user_id:
            found = True
    assert found

    response = client.get(
        f"{settings.API_V1_STR}/users/me", headers=normal_user_token_headers
    )
    assert response.status_code == 200
    found = False
    for proj in response.json()["projects"]:
        if proj["id"] == project_id:
            found = True
    assert found

    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/remove_users",
        headers=superuser_token_headers,
        json=[{"id": user_id}],
    )
    assert response.status_code == 200
    content = response.json()
    assert content["id"] == project_id
    assert len(content["users"]) == 1
    assert len(content["managers"]) == 1
    found = False
    for proj_user in content["users"]:
        if proj_user["id"] == user_id:
            found = True
    assert not found


def test_update_project_as_nomanager(
    client: TestClient,
    superuser_token_headers: dict[str, str],
    normal_user_token_headers: dict[str, str],
    db: Session,
) -> None:
    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=superuser_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    project_id = content["id"]

    response = client.get(
        f"{settings.API_V1_STR}/users/me", headers=normal_user_token_headers
    )
    assert response.status_code == 200
    user_id = response.json()["id"]

    response = client.post(
        f"{settings.API_V1_STR}/projects/{project_id}/add_users",
        headers=superuser_token_headers,
        json=[{"id": user_id}],
    )
    assert response.status_code == 200
    content = response.json()

    data2 = {"name": "Test Project 2"}
    response = client.put(
        f"{settings.API_V1_STR}/projects/{project_id}",
        headers=normal_user_token_headers,
        json=data2,
    )
    assert response.status_code == 403
    assert "You do not have manager access to project" in response.json()["detail"]


def test_project_export(
    client: TestClient, normal_user_token_headers: dict[str, str], db: Session
) -> None:
    # Get the user info
    response = client.get(
        f"{settings.API_V1_STR}/users/me", headers=normal_user_token_headers
    )
    assert response.status_code == 200
    user_id = response.json()["id"]
    user_email = response.json()["email"]
    user_name = response.json()["full_name"]

    data = {"name": "Test Project"}
    response = client.post(
        f"{settings.API_V1_STR}/projects",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    project_id = content["id"]

    # Make some content up
    for i in range(4):
        data = {
            "artifact_descriptor": "DESCRIPTOR_" + str(i),
            "artifact_type": "SOURCE_CODE_LOCATION",
            "artifact_priority": "HIGH",
            "submission_tool": "string",
            "type": "COMMENT",
            "contents": "SOME CONTENTS",
        }
        response = client.post(
            f"{settings.API_V1_STR}/projects/{project_id}/note",
            headers=normal_user_token_headers,
            json=data,
        )
        # Checking the note itself first
        assert response.status_code == 201
        last_artifact_id = response.json()["artifact_id"]

    for i in range(4):
        data = {
            "artifact_id": last_artifact_id,
            "artifact_type": "SOURCE_CODE_LOCATION",
            "artifact_priority": "HIGH",
            "submission_tool": "string",
            "type": "COMMENT",
            "contents": "SOME CONTENTS " + str(i),
        }
        response = client.post(
            f"{settings.API_V1_STR}/projects/{project_id}/note",
            headers=normal_user_token_headers,
            json=data,
        )
        # Checking the note itself first
        assert response.status_code == 201

    response = client.get(
        f"{settings.API_V1_STR}/projects/{project_id}/export",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert "project" in content
    assert content["project"]["name"] == "Test Project"

    assert "users" in content
    assert len(content["users"]) == 1
    assert content["users"][0]["id"] == user_id
    assert content["users"][0]["email"] == user_email
    assert content["users"][0]["full_name"] == user_name

    assert "artifacts" in content
    assert len(content["artifacts"]) == 4
    for artifact in content["artifacts"]:
        assert "notes" in artifact
        assert len(artifact["notes"]) == 5 or len(artifact["notes"]) == 1
        assert "descriptor" in artifact
        assert "DESCRIPTOR_" in artifact["descriptor"]
        assert artifact["priority"] == "HIGH"
        assert artifact["type"] == "SOURCE_CODE_LOCATION"
        assert "created_at" in artifact

        assert "notes" in artifact
        assert len(artifact["notes"]) > 0
        for note in artifact["notes"]:
            assert "author_id" in note
            assert "type" in note
            assert "contents" in note
            assert "submission_tool" in note
            assert "created_at" in note
