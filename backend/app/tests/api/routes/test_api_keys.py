from fastapi.testclient import TestClient
from sqlmodel import Session

from app.core.config import settings


def test_create_api_key(
    client: TestClient, superuser_token_headers: dict[str, str], db: Session
) -> None:
    data = {"description": "Anything"}
    response = client.post(
        f"{settings.API_V1_STR}/api_keys",
        headers=superuser_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    assert content["description"] == data["description"]
    assert "id" in content
    assert "owner_id" in content
    assert "client_id" in content
    assert "secret_key" in content
    assert "created_at" in content


def test_read_api_key_list_as_normal_user(
    client: TestClient, normal_user_token_headers: dict[str, str], db: Session
) -> None:
    data = {"description": "Key 1"}
    response = client.post(
        f"{settings.API_V1_STR}/api_keys",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201

    data = {"description": "Key 2"}
    response = client.post(
        f"{settings.API_V1_STR}/api_keys",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201

    response = client.get(
        f"{settings.API_V1_STR}/api_keys",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    # There should be at least 2 items, there could be more because other tests are
    # creating objects.
    assert len(content["data"]) >= 2
    assert content["count"] >= 2

    assert "description" in content["data"][0]
    assert "id" in content["data"][0]
    assert "owner_id" in content["data"][0]
    assert "client_id" in content["data"][0]
    assert (
        "secret_key" not in content["data"][0]
        or content["data"][0]["secret_key"] is None
    )
    assert "created_at" in content["data"][0]

    assert "description" in content["data"][1]
    assert "id" in content["data"][1]
    assert "owner_id" in content["data"][1]
    assert "client_id" in content["data"][1]
    assert (
        "secret_key" not in content["data"][1]
        or content["data"][1]["secret_key"] is None
    )
    assert "created_at" in content["data"][1]


def test_read_api_key_list_as_super_user(
    client: TestClient,
    superuser_token_headers: dict[str, str],
    normal_user_token_headers: dict[str, str],
    db: Session,
) -> None:
    data = {"description": "Key 1"}
    response = client.post(
        f"{settings.API_V1_STR}/api_keys",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201

    data = {"description": "Key 2"}
    response = client.post(
        f"{settings.API_V1_STR}/api_keys",
        headers=superuser_token_headers,
        json=data,
    )
    assert response.status_code == 201

    response = client.get(
        f"{settings.API_V1_STR}/api_keys",
        headers=superuser_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    # There should be at least 2 items, there could be more because other tests are
    # creating objects.
    assert len(content["data"]) >= 2
    assert content["count"] >= 2

    response = client.get(
        f"{settings.API_V1_STR}/api_keys",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    # There should be at least 1 items, there could be more because other tests are
    # creating objects.
    assert len(content["data"]) >= 1
    assert content["count"] >= 1


def test_read_api_key(
    client: TestClient, superuser_token_headers: dict[str, str], db: Session
) -> None:
    data = {"description": "Key Description"}
    response = client.post(
        f"{settings.API_V1_STR}/api_keys",
        headers=superuser_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    assert "id" in content
    api_key_id = content["id"]

    response = client.get(
        f"{settings.API_V1_STR}/api_keys/{api_key_id}",
        headers=superuser_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["description"] == data["description"]
    assert "id" in content
    assert "owner_id" in content
    assert "client_id" in content
    assert "secret_key" not in content or content["secret_key"] is None
    assert "created_at" in content


def test_read_api_key_not_found(
    client: TestClient, superuser_token_headers: dict[str, str], db: Session
) -> None:
    response = client.get(
        f"{settings.API_V1_STR}/api_keys/999999",
        headers=superuser_token_headers,
    )
    assert response.status_code == 404
    content = response.json()
    assert content["detail"] == "API key not found"


def test_read_item_not_enough_permissions(
    client: TestClient,
    superuser_token_headers: dict[str, str],
    normal_user_token_headers: dict[str, str],
    db: Session,
) -> None:
    data = {"description": "Key Description"}
    response = client.post(
        f"{settings.API_V1_STR}/api_keys",
        headers=superuser_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    assert "id" in content
    api_key_id = content["id"]

    response = client.get(
        f"{settings.API_V1_STR}/api_keys/{api_key_id}",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 404
    assert response.json()["detail"] == "API key not found"


def test_read_other_user_api_key_as_superuser(
    client: TestClient,
    superuser_token_headers: dict[str, str],
    normal_user_token_headers: dict[str, str],
    db: Session,
) -> None:
    data = {"description": "Key Description"}
    response = client.post(
        f"{settings.API_V1_STR}/api_keys",
        headers=normal_user_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    assert "id" in content
    api_key_id = content["id"]

    response = client.get(
        f"{settings.API_V1_STR}/api_keys/{api_key_id}",
        headers=superuser_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["description"] == data["description"]
    assert "id" in content
    assert "owner_id" in content
    assert "client_id" in content
    assert "secret_key" not in content or content["secret_key"] is None
    assert "created_at" in content


def test_delete_api_key(
    client: TestClient, superuser_token_headers: dict[str, str], db: Session
) -> None:
    data = {"description": "Key Description"}
    response = client.post(
        f"{settings.API_V1_STR}/api_keys",
        headers=superuser_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    assert "id" in content
    api_key_id = content["id"]

    response = client.delete(
        f"{settings.API_V1_STR}/api_keys/{api_key_id}",
        headers=superuser_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["message"] == "API key deleted successfully"


def test_delete_item_not_found(
    client: TestClient, superuser_token_headers: dict[str, str], db: Session
) -> None:
    response = client.delete(
        f"{settings.API_V1_STR}/api_keys/999",
        headers=superuser_token_headers,
    )
    assert response.status_code == 404
    content = response.json()
    assert content["detail"] == "API key not found"


def test_delete_item_not_enough_permissions(
    client: TestClient,
    superuser_token_headers: dict[str, str],
    normal_user_token_headers: dict[str, str],
    db: Session,
) -> None:
    data = {"description": "Key Description"}
    response = client.post(
        f"{settings.API_V1_STR}/api_keys",
        headers=superuser_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    assert "id" in content
    api_key_id = content["id"]

    response = client.delete(
        f"{settings.API_V1_STR}/api_keys/{api_key_id}",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 404
    content = response.json()
    assert content["detail"] == "API key not found"


def test_auth_via_api_key(
    client: TestClient, superuser_token_headers: dict[str, str], db: Session
) -> None:
    data = {"description": "Anything"}
    response = client.post(
        f"{settings.API_V1_STR}/api_keys",
        headers=superuser_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    client_id = content["client_id"]
    secret_key = content["secret_key"]

    response = client.get(
        f"{settings.API_V1_STR}/users/me",
        headers=superuser_token_headers,
    )
    assert response.status_code == 200
    user_content = response.json()

    response = client.get(
        f"{settings.API_V1_STR}/users/me",
        headers={"Authorization": "Bearer " + client_id + ":" + secret_key},
    )
    assert response.status_code == 200
    content = response.json()
    assert content == user_content


def test_auth_via_api_key_bad(
    client: TestClient, superuser_token_headers: dict[str, str], db: Session
) -> None:
    data = {"description": "Anything"}
    response = client.post(
        f"{settings.API_V1_STR}/api_keys",
        headers=superuser_token_headers,
        json=data,
    )
    assert response.status_code == 201
    content = response.json()
    client_id = content["client_id"]
    secret_key = content["secret_key"]

    response = client.get(
        f"{settings.API_V1_STR}/users/me",
        headers={"Authorization": "Bearer " + client_id + secret_key},
    )
    assert response.status_code == 401
    content = response.json()
    assert content["detail"] == "Could not validate credentials"

    response = client.get(
        f"{settings.API_V1_STR}/users/me",
        headers={"Authorization": "Bearer " + client_id[:-1] + ":" + secret_key},
    )
    assert response.status_code == 401
    content = response.json()
    assert content["detail"] == "Could not validate credentials"

    response = client.get(
        f"{settings.API_V1_STR}/users/me",
        headers={"Authorization": "Bearer " + client_id + ":" + secret_key[:-1]},
    )
    assert response.status_code == 401
    content = response.json()
    assert content["detail"] == "Could not validate credentials"

    response = client.get(
        f"{settings.API_V1_STR}/users/me",
        headers={"Authorization": "Bearer " + client_id + ":" + secret_key + ":"},
    )
    assert response.status_code == 401
    content = response.json()
    assert content["detail"] == "Could not validate credentials"


def test_read_api_key_list_pagination(
    client: TestClient, normal_user_token_headers: dict[str, str], db: Session
) -> None:
    response = client.get(
        f"{settings.API_V1_STR}/api_keys",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    # There should be at least 2 items, there could be more because other tests are
    # creating objects.
    assert len(content["data"]) is not None
    num_keys_to_start = content["count"]

    for i in range(10):
        data = {"description": f"Key {i}"}
        response = client.post(
            f"{settings.API_V1_STR}/api_keys",
            headers=normal_user_token_headers,
            json=data,
        )
        assert response.status_code == 201

    response = client.get(
        f"{settings.API_V1_STR}/api_keys",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["count"] == num_keys_to_start + 10

    response = client.get(
        f"{settings.API_V1_STR}/api_keys?limit=5",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert len(content["data"]) == 5
    assert content["count"] == num_keys_to_start + 10

    response = client.get(
        f"{settings.API_V1_STR}/api_keys?limit=99&skip=8",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert len(content["data"]) == num_keys_to_start + 10 - 8

    response = client.get(
        f"{settings.API_V1_STR}/api_keys?limit=99&sort=client_id&order=desc",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert len(content["data"]) == num_keys_to_start + 10
    last_client_id = ""
    for api_key in content["data"]:
        if last_client_id:
            assert api_key["client_id"] < last_client_id
        last_client_id = api_key["client_id"]

    response = client.get(
        f"{settings.API_V1_STR}/api_keys?limit=99&sort=client_id&order=asc",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert len(content["data"]) == num_keys_to_start + 10
    last_client_id = ""
    for api_key in content["data"]:
        if last_client_id:
            assert api_key["client_id"] > last_client_id
        last_client_id = api_key["client_id"]

    response = client.get(
        f"{settings.API_V1_STR}/api_keys?limit=99&sort=created_at&order=asc",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert len(content["data"]) == num_keys_to_start + 10
    last_created_at = ""
    for api_key in content["data"]:
        if last_created_at:
            assert api_key["created_at"] >= last_created_at
        last_created_at = api_key["created_at"]

    # I see you.
    response = client.get(
        f"{settings.API_V1_STR}/api_keys?limit=99&sort=secret_key_hash&order=asc",
        headers=normal_user_token_headers,
    )
    assert response.status_code == 422
