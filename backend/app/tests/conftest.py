from collections.abc import Generator

import pytest
from fastapi.testclient import TestClient
from sqlmodel import Session, delete

from app.core.config import settings
from app.core.db import engine, init_db
from app.main import app
from app.models import (
    ApiKey,
    Artifact,
    ArtifactNote,
    Project,
    ProjectManagerAccess,
    ProjectUserAccess,
    User,
)
from app.tests.utils.user import authentication_token_from_email
from app.tests.utils.utils import get_superuser_token_headers


# Note that running any tests will run this fixture, and basically clear out the
# entire database. This is fine for development and testing, but is something to
# be aware of.
@pytest.fixture(scope="session", autouse=True)
def db() -> Generator[Session, None, None]:
    with Session(engine) as session:
        init_db(session)
        yield session

        statement = delete(ArtifactNote)
        session.exec(statement)  # type: ignore

        statement = delete(Artifact)
        session.exec(statement)  # type: ignore

        statement = delete(ProjectManagerAccess)
        session.exec(statement)  # type: ignore

        statement = delete(ProjectUserAccess)
        session.exec(statement)  # type: ignore

        statement = delete(Project)
        session.exec(statement)  # type: ignore

        statement = delete(ApiKey)
        session.exec(statement)  # type: ignore

        statement = delete(User)
        session.exec(statement)  # type: ignore
        init_db(session)

        session.commit()


@pytest.fixture(scope="module")
def client() -> Generator[TestClient, None, None]:
    with TestClient(app) as c:
        yield c


@pytest.fixture(scope="module")
def superuser_token_headers(client: TestClient) -> dict[str, str]:
    return get_superuser_token_headers(client)


@pytest.fixture(scope="module")
def normal_user_token_headers(client: TestClient, db: Session) -> dict[str, str]:
    return authentication_token_from_email(
        client=client, email=settings.EMAIL_TEST_USER, db=db
    )
