import datetime as dt
from datetime import datetime
from typing import Any

from fastapi import APIRouter, Depends, HTTPException
from sqlmodel import func, select

from app.api.deps import (
    CurrentUser,
    SessionDep,
    get_artifact,
    get_project_as_user,
)
from app.models import (
    Artifact,
    ArtifactIn,
    ArtifactOut,
    ArtifactPriorityEnum,
    ArtifactSortEnum,
    ArtifactsOut,
    ArtifactTypeEnum,
    Message,
    OrderByEnum,
    Project,
)

router = APIRouter()


@router.get("/projects/{id}/artifacts", response_model=ArtifactsOut)
def read_project_artifacts(
    session: SessionDep,  # noqa
    current_user: CurrentUser,  # noqa
    skip: int = 0,
    limit: int = 100,
    updated_after: dt.datetime | None = None,
    updated_before: dt.datetime | None = None,
    sort: ArtifactSortEnum = ArtifactSortEnum.created_at,
    order: OrderByEnum = OrderByEnum.asc,
    filter: str = "",
    type: ArtifactTypeEnum = None,  # type: ignore[assignment]
    priority: ArtifactPriorityEnum = None,  # type: ignore[assignment]
    project: Project = Depends(get_project_as_user),
) -> Any:
    """
    Get a list of artifacts for a project.
    """

    if updated_after and updated_before:
        if updated_after > updated_before:
            raise HTTPException(
                status_code=400, detail="updated_after must be before updated_before"
            )

    sort_obj = getattr(Artifact, sort)
    if order == OrderByEnum.asc:
        sort_obj = sort_obj.asc()
    else:
        sort_obj = sort_obj.desc()

    if not updated_after:
        updated_after = datetime.min

    if not updated_before:
        updated_before = datetime.max

    statement = (
        select(Artifact)
        .where(Artifact.project_id == project.id)
        .where(Artifact.updated_at >= updated_after)
        .where(Artifact.updated_at <= updated_before)
    )
    count_statement = (
        select(func.count())
        .select_from(Artifact)
        .where(Artifact.project_id == project.id)
        .where(Artifact.updated_at >= updated_after)
        .where(Artifact.updated_at <= updated_before)
    )

    if filter:
        statement = statement.where(Artifact.descriptor.ilike(f"%{filter}%"))  # type: ignore[attr-defined]
        count_statement = count_statement.where(
            Artifact.descriptor.ilike(f"%{filter}%")  # type: ignore[attr-defined]
        )
    if type:
        statement = statement.where(Artifact.type == type)
        count_statement = count_statement.where(Artifact.type == type)
    if priority:
        statement = statement.where(Artifact.priority == priority)
        count_statement = count_statement.where(Artifact.priority == priority)

    count = session.exec(count_statement).one()

    statement = statement.offset(skip).limit(limit).order_by(sort_obj)

    artifact_out_list = []
    artifacts = session.exec(statement).all()
    for artifact in artifacts:
        note_ids_list = []
        for note in artifact.notes:
            note_ids_list.append(note.id)

        out = ArtifactOut.model_validate(artifact, update={"note_ids": note_ids_list})
        artifact_out_list.append(out)

    return ArtifactsOut(data=artifact_out_list, count=count)


@router.get("/artifacts/{id}", response_model=ArtifactOut, status_code=200)
def read_artifact(
    session: SessionDep,  # noqa
    current_user: CurrentUser,  # noqa
    artifact: Artifact = Depends(get_artifact),
) -> Any:
    """
    Get a specific artifact by ID. Includes basic note information.
    """
    note_ids_list = []
    for note in artifact.notes:
        note_ids_list.append(note.id)
    out = ArtifactOut.model_validate(artifact, update={"note_ids": note_ids_list})
    return out


@router.put("/artifacts/{id}", response_model=ArtifactOut, status_code=200)
def update_artifact(
    session: SessionDep,
    current_user: CurrentUser,  # noqa
    artifact_in: ArtifactIn,
    artifact: Artifact = Depends(get_artifact),
) -> Any:
    """
    Update a specific artifact by ID.
    """
    update_dict = artifact_in.model_dump(exclude_unset=True)
    artifact.sqlmodel_update(update_dict)
    artifact.updated_at = datetime.utcnow()

    session.add(artifact)
    session.commit()
    session.refresh(artifact)

    note_ids_list = []
    for note in artifact.notes:
        note_ids_list.append(note.id)
    out = ArtifactOut.model_validate(artifact, update={"note_ids": note_ids_list})
    return out


@router.delete("/artifacts/{id}", response_model=Message, status_code=200)
def delete_artifact(
    session: SessionDep,
    current_user: CurrentUser,  # noqa
    artifact: Artifact = Depends(get_artifact),
) -> Any:
    """
    Delete a specific artifact by ID.
    """
    for note in artifact.notes:
        session.delete(note)

    session.delete(artifact)
    session.commit()

    return {"message": "Artifact deleted"}
