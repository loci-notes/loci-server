from datetime import datetime, timezone
from typing import Any

from fastapi import APIRouter

from app.core.config import settings
from app.models import StatusOut
from app.version import __version__

router = APIRouter()


@router.get("/status", response_model=StatusOut)
def status() -> Any:
    """
    Return the status of the running API.
    """
    return {
        "version": __version__,
        "env": settings.ENVIRONMENT,
        "now": datetime.now(timezone.utc),
        "open_reg": settings.USERS_OPEN_REGISTRATION,
    }
