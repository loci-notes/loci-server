from typing import Any

from fastapi import APIRouter, HTTPException
from sqlmodel import func, select

from app.api.deps import CurrentUser, SessionDep
from app.core.security import get_password_hash
from app.models import (
    ApiKey,
    ApiKeyIn,
    ApiKeyOut,
    ApiKeySortEnum,
    ApiKeysOut,
    Message,
    OrderByEnum,
)
from app.utils import generate_client_id, generate_secret_key

router = APIRouter()


@router.get("/", response_model=ApiKeysOut)
def read_api_keys(
    session: SessionDep,
    current_user: CurrentUser,
    skip: int = 0,
    limit: int = 100,
    sort: ApiKeySortEnum = ApiKeySortEnum.created_at,
    order: OrderByEnum = OrderByEnum.desc,
) -> Any:
    """
    Retrieve all API Keys to which a user has access. None include the secret key.
    """
    sort_obj = getattr(ApiKey, sort)
    if order == OrderByEnum.asc:
        sort_obj = sort_obj.asc()
    else:
        sort_obj = sort_obj.desc()

    if current_user.is_superuser:
        count_statement = select(func.count()).select_from(ApiKey)
        count = session.exec(count_statement).one()
        statement = select(ApiKey).offset(skip).limit(limit).order_by(sort_obj)
        api_keys = session.exec(statement).all()
    else:
        count_statement = (
            select(func.count())
            .select_from(ApiKey)
            .where(ApiKey.owner_id == current_user.id)
        )
        count = session.exec(count_statement).one()
        statement = (
            select(ApiKey)
            .where(ApiKey.owner_id == current_user.id)
            .offset(skip)
            .limit(limit)
            .order_by(sort_obj)
        )
        api_keys = session.exec(statement).all()

    return ApiKeysOut(data=api_keys, count=count)


@router.get("/{id}", response_model=ApiKeyOut)
def read_api_key(session: SessionDep, current_user: CurrentUser, id: int) -> Any:
    """
    Get an API key by ID. It does not include the secret key.
    """
    api_key = session.get(ApiKey, id)
    if not api_key or (
        not current_user.is_superuser and (api_key.owner_id != current_user.id)
    ):
        raise HTTPException(status_code=404, detail="API key not found")
    return api_key


@router.post("/", response_model=ApiKeyOut, status_code=201)
def create_api_key(
    *, session: SessionDep, current_user: CurrentUser, api_key_in: ApiKeyIn
) -> Any:
    """
    Create new API key. This is the only time it includes the secret key.
    """
    # This gens a sufficiently secure value so it *should* be unique, but we can
    # probably check. TODO
    client_id = generate_client_id()
    secret_key = generate_secret_key()
    secret_key_hash = get_password_hash(secret_key)
    api_key = ApiKey.model_validate(
        api_key_in,
        update={
            "owner_id": current_user.id,
            "client_id": client_id,
            "secret_key_hash": secret_key_hash,
        },
    )
    session.add(api_key)
    session.commit()
    session.refresh(api_key)

    api_key_out = ApiKeyOut.model_validate(api_key)
    api_key_out.secret_key = secret_key
    return api_key_out


# There is no way to update an API key, only delete it and create a new one.
@router.delete("/{id}")
def delete_api_key(session: SessionDep, current_user: CurrentUser, id: int) -> Message:
    """
    Delete an API key.
    """
    api_key = session.get(ApiKey, id)
    if not api_key or (
        not current_user.is_superuser and (api_key.owner_id != current_user.id)
    ):
        raise HTTPException(status_code=404, detail="API key not found")
    session.delete(api_key)
    session.commit()
    return Message(message="API key deleted successfully")
