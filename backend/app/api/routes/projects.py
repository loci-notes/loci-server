from datetime import datetime, timezone
from typing import Any

from fastapi import APIRouter, Depends, HTTPException
from sqlmodel import func, select

from app.api.deps import (
    CurrentUser,
    SessionDep,
    get_project_as_manager,
    get_project_as_user,
)
from app.crud import get_all_project_notes
from app.models import (
    OrderByEnum,
    Project,
    ProjectAccessUser,
    ProjectExport,
    ProjectIn,
    ProjectOut,
    ProjectSortEnum,
    ProjectsOut,
    User,
)

router = APIRouter()


@router.post("/projects/", response_model=ProjectOut, status_code=201)
def create_project(
    session: SessionDep, current_user: CurrentUser, project_in: ProjectIn
) -> Any:
    """
    Create a new project, and add the creator as both a user and a manager.
    """
    new_project = Project.model_validate(project_in)
    new_project.managers.append(current_user)
    new_project.users.append(current_user)

    # TODO Add project_access
    session.add(new_project)
    session.commit()
    session.refresh(new_project)

    return new_project


@router.get("/projects/{id}", response_model=ProjectOut)
def read_project(
    session: SessionDep,  # noqa
    current_user: CurrentUser,  # noqa
    project: Project = Depends(get_project_as_user),
) -> Any:
    """
    Get a specific project by ID.
    """
    return project


@router.get("/projects/", response_model=ProjectsOut)
def read_projects(
    session: SessionDep,
    current_user: CurrentUser,
    skip: int = 0,
    limit: int = 100,
    sort: ProjectSortEnum = ProjectSortEnum.created_at,
    order: OrderByEnum = OrderByEnum.asc,
) -> Any:
    """
    Gets a list of projects to which the user has any level of access.
    """
    sort_obj = getattr(Project, sort)
    if order == OrderByEnum.asc:
        sort_obj = sort_obj.asc()
    else:
        sort_obj = sort_obj.desc()

    if current_user.is_superuser:
        count_statement = select(func.count()).select_from(Project)
        count = session.exec(count_statement).one()

        statement = select(Project).offset(skip).limit(limit).order_by(sort_obj)
        projects = session.exec(statement).all()
    else:
        count_statement = (
            select(func.count())
            .select_from(Project)
            .where(Project.users.contains(current_user))  # type: ignore[attr-defined]
        )
        count = session.exec(count_statement).one()

        statement = (
            select(Project)
            .where(Project.users.contains(current_user))  # type: ignore[attr-defined]
            .offset(skip)
            .limit(limit)
            .order_by(sort_obj)
        )
        projects = session.exec(statement).all()

    return ProjectsOut(data=projects, count=count)


@router.put("/projects/{id}", response_model=ProjectOut)
def update_project(
    session: SessionDep,
    current_user: CurrentUser,  # noqa
    project_in: ProjectIn,
    project: Project = Depends(get_project_as_manager),
) -> Any:
    """
    Update a project. Only managers can update a project.
    """
    update_dict = project_in.model_dump(exclude_unset=True)
    project = project.sqlmodel_update(update_dict)
    session.add(project)
    session.commit()
    session.refresh(project)
    return project


@router.post("/projects/{id}/add_users", response_model=ProjectOut)
def update_project_access(
    session: SessionDep,
    current_user: CurrentUser,  # noqa
    new_users: list[ProjectAccessUser],
    project: Project = Depends(get_project_as_manager),
) -> Any:
    """
    Add users access to a project. Only managers can update a project.
    """
    for user_access_obj in new_users:
        user = session.get(User, user_access_obj.id)
        if user is None:
            raise HTTPException(status_code=404, detail="User not found")
        if user not in project.users:
            project.users.append(user)
        if user not in project.managers and user_access_obj.is_manager:
            project.managers.append(user)

    session.add(project)
    session.commit()
    session.refresh(project)
    return project


@router.post("/projects/{id}/remove_users", response_model=ProjectOut)
def delete_project_access(
    session: SessionDep,
    current_user: CurrentUser,  # noqa
    remove_users: list[ProjectAccessUser],
    project: Project = Depends(get_project_as_manager),
) -> Any:
    """
    Remove users with access to a project. Only managers can remove users from
    a project.
    """
    for user_access_obj in remove_users:
        user = session.get(User, user_access_obj.id)

        if user in project.users:
            project.users.remove(user)
        if user in project.managers:
            project.managers.remove(user)

    session.add(project)
    session.commit()
    session.refresh(project)
    return project


@router.delete("/projects/{id}")
def delete_project(
    session: SessionDep,
    current_user: CurrentUser,  # noqa
    project: Project = Depends(get_project_as_manager),
) -> Any:
    """
    Delete a project. Only managers can delete a project.
    """
    # TODO delete all attached artifacts.
    session.delete(project)
    session.commit()
    return {"message": "Project deleted"}


@router.get("/projects/{id}/export", response_model=ProjectExport)
def export_project(
    session: SessionDep,  # noqa
    current_user: CurrentUser,  # noqa
    project: Project = Depends(get_project_as_user),
) -> Any:
    """
    Export a project to a JSON format.
    """
    project_notes = get_all_project_notes(session=session, project=project)

    return ProjectExport(
        project=project,
        export_time=datetime.now(timezone.utc),
        users=project.users,
        managers=project.managers,
        artifacts=project.artifacts,
        notes=project_notes,
    )
