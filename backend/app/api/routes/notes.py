from datetime import datetime
from typing import Any

from fastapi import APIRouter, Depends, HTTPException
from sqlmodel import select

from app.api.deps import CurrentUser, SessionDep, get_note, get_project_as_user
from app.models import (
    Artifact,
    ArtifactNote,
    ArtifactNoteIn,
    ArtifactNoteUpdateIn,
    ArtifactPriorityEnum,
    ArtifactTypeEnum,
    Message,
    Project,
)

router = APIRouter()


@router.post("/projects/{id}/note", response_model=ArtifactNote, status_code=201)
def create_note(
    session: SessionDep,
    current_user: CurrentUser,
    note_in: ArtifactNoteIn,
    project: Project = Depends(get_project_as_user),
) -> Any:
    """
    Create a note attached to some artifact in the project. The artifact is specified
    via either the descriptor or ID, and if by descriptor, and it does not already
    exist, it will be created. If the note is a duplicate, it will not be created,
    and the pre-existing note will be returned, with no error.
    """

    # First determine if the artifact this is attaching to refers to a artifact
    # that already exists or not.
    if note_in.artifact_id is not None:
        # Get the artifact by ID. Easy.
        artifact_id = note_in.artifact_id
        artifact = session.get(Artifact, artifact_id)
    elif note_in.artifact_descriptor is not None:
        # Lookup the artifact by descriptor. If it does not exist, create it.
        statement = (
            select(Artifact)
            .where(Artifact.descriptor == note_in.artifact_descriptor)
            .where(Artifact.project_id == project.id)
        )
        # There should only ever be zero or one.
        artifact = session.exec(statement).first()

        if artifact is None:
            # This artifact does not yet exist. Create the artifact. These
            # default values will be overridden later by the note (if set).
            artifact = Artifact(
                descriptor=note_in.artifact_descriptor,
                project_id=project.id,
                type=ArtifactTypeEnum.OTHER,
                priority=ArtifactPriorityEnum.MEDIUM,
            )

            session.add(artifact)
            session.commit()
            session.refresh(artifact)
    else:
        raise HTTPException(
            status_code=400,
            detail="Either artifact_id or artifact_descriptor must be specified.",
        )

    if artifact is None:
        raise HTTPException(status_code=404, detail="Artifact not found")

    # Detect and prevent duplicate notes (very helpful for importing, can run the
    # importer multiple times without worrying about duplicates). We match based
    # on artifact ID, contents, type, and tool. We do not need to match on priority.
    statement = (
        select(ArtifactNote)  # type: ignore[assignment]
        .where(ArtifactNote.artifact_id == artifact.id)
        .where(ArtifactNote.contents == note_in.contents)
        .where(ArtifactNote.type == note_in.type)
        .where(ArtifactNote.submission_tool == note_in.submission_tool)
    )
    existing_note = session.exec(statement).first()
    if existing_note is not None:
        # This note already exists. We could throw an error message, but honestly it's
        # not worth it, just return now.
        return existing_note

    # Now that we have the artifact, and we know this note is new/unique, create
    # the note.
    new_note = ArtifactNote(
        artifact_id=artifact.id,
        type=note_in.type,
        author_id=current_user.id,
        contents=note_in.contents,
    )

    # If the type or priority are specified, set them.
    if note_in.artifact_type is not None:
        artifact.type = note_in.artifact_type

    if note_in.artifact_priority is not None:
        artifact.priority = note_in.artifact_priority

    if note_in.submission_tool is not None:
        new_note.submission_tool = note_in.submission_tool

    # Because we are creating a new note, we need to update the artifact's updated_at
    # field.
    artifact.updated_at = new_note.created_at

    session.add(artifact)
    session.add(new_note)
    session.commit()
    session.refresh(artifact)
    session.refresh(new_note)
    return new_note


@router.get("/notes/{id}", response_model=ArtifactNote)
def read_note(
    session: SessionDep,  # noqa
    note: ArtifactNote = Depends(get_note),
) -> Any:
    """
    Read note by ID.
    """
    return note


@router.put("/notes/{id}", response_model=ArtifactNote)
def update_note(
    session: SessionDep,
    note_in: ArtifactNoteUpdateIn,
    note: ArtifactNote = Depends(get_note),
) -> Any:
    """
    Update note by ID.
    """
    update_dict = note_in.model_dump(exclude_unset=True)
    note.sqlmodel_update(update_dict)
    note.updated_at = datetime.utcnow()

    # Because we are creating a new note, we need to update the artifact's updated_at
    # field.
    # Lookup the artifact by descriptor. If it does not exist, create it.
    statement = select(Artifact).where(Artifact.id == note.artifact_id)
    # There should only ever be one.
    artifact = session.exec(statement).first()
    if artifact is None:
        raise HTTPException(status_code=404, detail="Artifact not found")
    artifact.updated_at = note.updated_at

    session.add(note)
    session.add(artifact)
    session.commit()
    session.refresh(artifact)
    session.refresh(note)

    return note


@router.delete("/notes/{id}", response_model=Message)
def delete_note(
    session: SessionDep,
    note: ArtifactNote = Depends(get_note),
) -> Any:
    """
    Delete note by ID.
    """
    artifact_id = note.artifact_id

    # Because we are creating a new note, we need to update the artifact's updated_at
    # field.
    statement = select(Artifact).where(Artifact.id == artifact_id)
    # There should only ever be one.
    artifact = session.exec(statement).first()
    if artifact is None:
        raise HTTPException(status_code=404, detail="Artifact not found")
    artifact.updated_at = datetime.utcnow()

    session.add(artifact)
    session.delete(note)
    session.commit()
    return {"message": "Note deleted"}
