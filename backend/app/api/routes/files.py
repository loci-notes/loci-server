import zipfile
from datetime import timedelta
from pathlib import Path
from typing import Any

from fastapi import APIRouter, Depends, HTTPException, UploadFile
from minio import Minio
from minio.versioningconfig import VersioningConfig
from sqlmodel import select

from app.api.deps import CurrentUser, SessionDep, get_artifact, get_project_as_manager
from app.core.config import settings
from app.models import (
    Artifact,
    ArtifactFileLocationOut,
    ArtifactNote,
    ArtifactNoteTypeEnum,
    ArtifactPriorityEnum,
    ArtifactTypeEnum,
    Message,
    Project,
)

router = APIRouter()


def get_storage_client() -> Minio:
    # Upload all files to the storage backend.
    storage_client = Minio(
        settings.STORAGE_ENDPOINT,
        access_key=settings.STORAGE_ACCESS_KEY,
        secret_key=settings.STORAGE_SECRET_KEY,
        secure=settings.STORAGE_HTTPS,
    )

    bucket_name = settings.STORAGE_BUCKET

    # Make the bucket if it doesn't exist.
    if not storage_client.bucket_exists(bucket_name):
        storage_client.make_bucket(bucket_name)
        # We should probably check this, but we're not going to for now.
        storage_client.set_bucket_versioning(bucket_name, VersioningConfig("Enabled"))
    return storage_client


@router.post("/projects/{id}/zipfile", response_model=Message, status_code=202)
def upload_project_zipfile(
    session: SessionDep,
    current_user: CurrentUser,
    file: UploadFile,
    project: Project = Depends(get_project_as_manager),
) -> Any:
    """
    Upload a zipfile to a project, creating an artifact for it if needed. Only managers
    can upload files to a project.
    """
    # First make sure this file has not already been uploaded.
    statement = (
        select(Artifact)
        .where(Artifact.project_id == project.id)
        .where(Artifact.descriptor == file.filename)
    )
    existing_artifact = session.exec(statement).first()
    if existing_artifact is not None:
        raise HTTPException(status_code=400, detail="File already uploaded.")

    # In case anyone wonders why we use Python 3.11 instead of 3.10, it's because
    # 3.11 has a new feature that allows us to use the zipfile with a
    # SpooledTemporaryFile which is a file-like object that can be used with the
    # zipfile module. This was fixed in 3.11 per https://stackoverflow.com/a/74244579
    try:
        with zipfile.ZipFile(file.file, mode="r") as archive:
            fileslist = archive.filelist

        # Get a list of all the root directories in all previous zipfiles
        # for this project, so we know that none will conflict.
        metadata_notes = []
        get_file_artifacts_statement = (
            select(Artifact)
            .where(Artifact.project_id == project.id)
            .where(Artifact.type == ArtifactTypeEnum.SOURCE_CODE_FILE)
        )
        project_file_artifacts = session.exec(get_file_artifacts_statement).all()

        for artifact in project_file_artifacts:
            for note in artifact.notes:
                if (
                    note.type == ArtifactNoteTypeEnum.METADATA_KV
                    and note.contents.startswith("ZIPFILE_REPO:")
                ):
                    metadata_notes.append(note.contents)

        # Iterate over all files in the zipfile to see if there are any non-directory
        # files stored in the root
        for fileinfo in fileslist:
            if fileinfo.is_dir():
                # TODO Technically there's a TOCTOU problem where the same repo could
                # be uploaded twice in two different zipfiles at roughly the same time.
                # We should fix this eventually.
                repo_name = fileinfo.filename.split("/")[0]
                if "ZIPFILE_REPO:" + repo_name in metadata_notes:
                    raise HTTPException(
                        status_code=400,
                        detail=f"This ZIP file contains the {repo_name} repo which "
                        "was already uploaded in another ZIP file. You cannot "
                        "upload it again.",
                    )
                # Good to go.
                pass
            else:
                filename = fileinfo.filename
                filepath = Path(filename)
                if len(filepath.parts) == 1:
                    raise HTTPException(
                        status_code=400,
                        detail="All files in the zipfile must be in a directory.",
                    )
        storage_client = get_storage_client()

        bucket_name = settings.STORAGE_BUCKET
        location = f"{project.id}/{file.filename}"

        file.file.seek(0)
        filesize = file.size

        if filesize is None or filesize == 0:
            raise HTTPException(
                status_code=400, detail="File is empty or size could not be determined."
            )

        obj_out = storage_client.put_object(
            bucket_name,
            location,
            file.file,
            content_type="application/zip",
            length=filesize,
        )

        if not obj_out:
            raise HTTPException(
                status_code=400, detail="Failed to upload file to storage backend."
            )

        # Make an artifact for the file.
        artifact = Artifact(
            descriptor=file.filename,
            project_id=project.id,
            type=ArtifactTypeEnum.SOURCE_CODE_FILE,
            priority=ArtifactPriorityEnum.NONE,
        )

        session.add(artifact)
        session.commit()
        session.refresh(artifact)

        contents = f"ZIPFILE_LOCATION:{location}"
        new_note = ArtifactNote(
            artifact_id=artifact.id,
            type=ArtifactNoteTypeEnum.METADATA_KV,
            author_id=current_user.id,
            contents=contents,
        )

        session.add(new_note)
        session.commit()

        # We need to store the root directories that were in the zipfile so that
        # we know if any conflict later.
        for fileinfo in fileslist:
            if fileinfo.is_dir():
                # See if it's a root directory.
                file_path = Path(fileinfo.filename)
                if len(file_path.parts) == 1:
                    # This is a root directory.
                    repo_name = fileinfo.filename.split("/")[0]
                    contents = f"ZIPFILE_REPO:{repo_name}"
                    new_note = ArtifactNote(
                        artifact_id=artifact.id,
                        type=ArtifactNoteTypeEnum.METADATA_KV,
                        author_id=current_user.id,
                        contents=contents,
                    )

                    session.add(new_note)
                    session.commit()

    except zipfile.BadZipFile:
        raise HTTPException(status_code=400, detail="Zip file is invalid.")
    except HTTPException as e:
        raise e
    except Exception as e:
        raise HTTPException(status_code=400, detail=str(e))

    return {"message": "File uploaded."}


@router.get("/artifacts/{id}/file", response_model=ArtifactFileLocationOut)
def get_artifact_file(
    session: SessionDep,
    current_user: CurrentUser,
    artifact: Artifact = Depends(get_artifact),
) -> Any:
    if artifact.type != ArtifactTypeEnum.SOURCE_CODE_FILE:
        raise HTTPException(status_code=400, detail="Artifact is not a file.")

    storage_client = get_storage_client()
    bucket_name = settings.STORAGE_BUCKET

    file_location = artifact.descriptor
    location = f"{artifact.project_id}/{file_location}"

    file_link = storage_client.get_presigned_url(
        "GET", bucket_name, location, expires=timedelta(minutes=5)
    )

    return ArtifactFileLocationOut(link=file_link, filename=artifact.descriptor)
