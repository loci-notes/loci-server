from collections.abc import Generator
from typing import Annotated

from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from jose import JWTError, jwt
from pydantic import ValidationError
from sqlmodel import Session, select

from app.core import security
from app.core.config import settings
from app.core.db import engine
from app.models import (
    ApiKey,
    Artifact,
    ArtifactNote,
    Project,
    ProjectManagerAccess,
    TokenPayload,
    User,
)

reusable_oauth2 = OAuth2PasswordBearer(
    tokenUrl=f"{settings.API_V1_STR}/login/access-token"
)


def get_db() -> Generator[Session, None, None]:
    with Session(engine) as session:
        yield session


SessionDep = Annotated[Session, Depends(get_db)]
TokenDep = Annotated[str, Depends(reusable_oauth2)]


def get_current_user(
    session: SessionDep,
    token: TokenDep,
) -> User:
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
    )

    if ":" not in token:
        # Authenticate via JWT
        try:
            payload = jwt.decode(
                token, settings.SECRET_KEY, algorithms=[security.ALGORITHM]
            )
            token_data = TokenPayload(**payload)
        except (JWTError, ValidationError):
            raise credentials_exception
        user = session.get(User, token_data.sub)
    else:
        # Authenticate via API key
        # The API key should be passed as `<client_id>:<secret_key>`
        api_key_parts = token.split(":")
        if len(api_key_parts) != 2:
            raise credentials_exception
        client_id, secret_key = api_key_parts
        statement = select(ApiKey).where(ApiKey.client_id == client_id)
        api_key = session.exec(statement).first()
        if not api_key:
            raise credentials_exception
        if not security.verify_password(secret_key, api_key.secret_key_hash):
            raise credentials_exception
        user = session.get(User, api_key.owner_id)

    if not user:
        raise HTTPException(status_code=404, detail="User not found")
    if not user.is_active:
        raise HTTPException(status_code=400, detail="Inactive user")
    return user


CurrentUser = Annotated[User, Depends(get_current_user)]


def get_current_active_superuser(current_user: CurrentUser) -> User:
    if not current_user.is_superuser:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="The user doesn't have enough privileges",
        )
    return current_user


# This will make sure a user has access (of any level) to the project they are
# attempting to access
def get_project_as_user(
    id: int,
    session: SessionDep,
    current_user: User = Depends(get_current_user),
) -> Project:
    # First ensure this project exists
    # Get project object.
    project = session.get(Project, id)
    if project is None:
        raise HTTPException(status.HTTP_404_NOT_FOUND, detail="Project not found")

    # Next check to ensure the user has permission to the project.
    if current_user in project.users:
        return project
    else:
        raise HTTPException(
            status.HTTP_403_FORBIDDEN,
            detail="You do not have access to project %s."
            " Please contact a project manager for access." % project.id,
        )


# This will make sure a user has manager-level access to the project they are
# attempting to access
def get_project_as_manager(
    session: SessionDep,
    project: Project = Depends(get_project_as_user),
    current_user: User = Depends(get_current_user),
) -> Project:
    statement = (
        select(ProjectManagerAccess)
        .where(ProjectManagerAccess.user_id == current_user.id)
        .where(ProjectManagerAccess.project_id == project.id)
    )

    results = session.exec(statement).all()
    if results is None or len(results) != 1:
        raise HTTPException(
            status.HTTP_403_FORBIDDEN,
            detail="You do not have manager access to project %s."
            " Please contact a project manager for access." % project.id,
        )
    return project


def get_artifact(
    session: SessionDep,
    id: int,
    current_user: User = Depends(get_current_user),
) -> Artifact:
    """
    This will ensure the user has access to the artifact they are attempting to
    access. If so, it returns the artifact. If not, it raises a 404 HTTPException.
    """
    artifact = session.get(Artifact, id)
    if artifact is None:
        raise HTTPException(status.HTTP_404_NOT_FOUND, detail="Artifact not found")
    project = session.get(Project, artifact.project_id)
    if project is None or project.id is None:
        # Technically this should be a 403 to indicate the user trying to do
        # something with a project they don't have access to, but we'll just
        # say the artifact doesn't exist.
        raise HTTPException(status.HTTP_404_NOT_FOUND, detail="Artifact not found")

    # Permissions check. Will raise a 403 if the user doesn't have access to the
    # project.
    project = get_project_as_user(project.id, session, current_user)

    return artifact


def get_note(
    session: SessionDep,
    id: int,
    current_user: User = Depends(get_current_user),
) -> ArtifactNote:
    """
    This will ensure the user has access to the note they are attempting to
    access. If so, it returns the note. If not, it raises an 404 HTTPException.
    """
    note = session.get(ArtifactNote, id)
    if note is None:
        raise HTTPException(status.HTTP_404_NOT_FOUND, detail="Note not found")
    artifact = session.get(Artifact, note.artifact_id)
    if artifact is None or artifact.id is None:
        # Technically this should be a 403 to indicate the user trying to do
        # something with a project they don't have access to, but we'll just
        # say the note doesn't exist.
        raise HTTPException(status.HTTP_404_NOT_FOUND, detail="Note not found")

    artifact = get_artifact(session, artifact.id, current_user)

    return note
