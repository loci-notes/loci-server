from fastapi import APIRouter

from app.api.routes import (
    api_keys,
    artifacts,
    files,
    login,
    notes,
    projects,
    status,
    users,
    utils,
)

api_router = APIRouter()
api_router.include_router(login.router)
api_router.include_router(status.router)
api_router.include_router(users.router, prefix="/users")
api_router.include_router(api_keys.router, prefix="/api_keys")
api_router.include_router(utils.router, prefix="/utils")
api_router.include_router(projects.router)
api_router.include_router(artifacts.router)
api_router.include_router(notes.router)
api_router.include_router(files.router)
