from fastapi import FastAPI
from fastapi.routing import APIRoute
from starlette.middleware.cors import CORSMiddleware

from app.api.main import api_router
from app.core.config import settings
from app.version import __version__


def custom_generate_unique_id(route: APIRoute) -> str:
    return f"{route.name}"


app = FastAPI(
    title="Loci Notes Server",
    openapi_url=f"{settings.API_V1_STR}/openapi.json",
    generate_unique_id_function=custom_generate_unique_id,
    contact={
        "name": "TheTwitchy",
        "url": "https://loci-notes.gitlab.io/",
        "email": "thetwitchy@thetwitchy.com",
    },
    license_info={
        "name": "GNU General Public License v3.0",
        "url": "https://gitlab.com/loci-notes/loci-server/-/blob/main/LICENSE",
    },
    summary="The core server of the Loci Notes system.",
    version=__version__,
    description="""
The [Loci Notes](https://loci-notes.gitlab.io/) server API.

The Loci Notes server is a FastAPI server that provides a RESTful API for interacting with the Loci Notes system. The server is designed to be used by the Loci Notes web UI, VS Code extension, Loci Notes CLI, and any other clients that may be developed in the future.""",  # noqa
)

# Set all CORS enabled origins
if settings.BACKEND_CORS_ORIGINS:
    app.add_middleware(
        CORSMiddleware,
        allow_origins=[
            str(origin).strip("/") for origin in settings.BACKEND_CORS_ORIGINS
        ],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

app.include_router(api_router, prefix=settings.API_V1_STR)
